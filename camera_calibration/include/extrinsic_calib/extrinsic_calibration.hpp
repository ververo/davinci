/*
 * stereo_calibration.hpp
 *
 *  Created on: March 5, 2014
 *      Author: Veronica Penza
 */


#ifndef __EXTRINSICCALIBRATION_HPP_
#define __EXTRINSICCALIBRATION_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <math.h>


//OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv/cxcore.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/image_encodings.h>

#include "setting.hpp"


class ExtrinsicCalibration
{

public:

	std::string path;

	ros::NodeHandle n;
	image_transport::ImageTransport it;
	//	image_transport::Subscriber imgL_SUB;
	cv::Mat imgL;
	tf::TransformBroadcaster broadcaster;

	// variables
	cv::Mat cameraMatrix, distCoeffs;
	cv::Size imageSize;
	std::vector<cv::Point2f> corners;
	cv::Mat rvec, tvec, axis;
	int counter_frames;
	int max_n_frames;
	bool calib_go;
	bool endCal;
	bool found;
	char key;
	std::string inputSettingsFile;
	std::string intrinsicsFile;
	Settings s;
	int chessBoardFlags;

	// methods
	ExtrinsicCalibration();
	bool initParams();
	void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
			Settings::Pattern patternType /*= Settings::CHESSBOARD*/);



};//End of class CameraCalibration


#endif //__EXTRINSICCALIBRATION_HPP_
