/*
 * stereo_calibration.hpp
 *
 *  Created on: March 5, 2014
 *      Author: Veronica Penza
 */


#ifndef __INTRINSICCALIBRATION_HPP_
#define __INTRINSICCALIBRATION_HPP_

#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <math.h>

//OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv/cxcore.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "setting.hpp"
#include "rectification.hpp"



using namespace cv;
using namespace std;
using namespace sensor_msgs;
using namespace sensor_msgs::image_encodings;
using namespace message_filters;


class IntrinsicCalibration
{

public:

	//	std::string path_root;
	//	std::string path_configFile;
	ros::NodeHandle n;

	IntrinsicCalibration();

	enum { DETECTION = 0, CAPTURING = 1,  CALIBRATED = 2 };

	bool capturing_step;
	bool stereocalibration_sep;

	bool init();
	void help();
	bool runCalibration( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
			vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs,
			vector<float>& reprojErrs,  double& totalAvgErr);
	bool runStereoCalibration( Settings& s, Size& imageSize,
			Mat& cameraMatrixL, Mat& distCoeffsL,
			vector<vector<Point2f> > imagePointsL,
			Mat& cameraMatrixR, Mat& distCoeffsR,vector<vector<Point2f> > imagePointsR,
			cv::Mat& R, cv::Mat& T, cv::Mat& E, cv::Mat& F,
			float& reprojErrs,  double& totalAvgErr);

	bool runCalibrationAndSave(Settings& s, Size imageSize, Mat&  cameraMatrix, Mat& distCoeffs,
			vector<vector<Point2f> > imagePoints );
	bool runStereoCalibrationAndSave(Settings& s, Size imageSize,
			Mat&  cameraMatrixL, Mat& distCoeffsL,
			vector<vector<Point2f> > imagePointsL,
			Mat&  cameraMatrixR, Mat& distCoeffsR,
			vector<vector<Point2f> > imagePointsR);
	static double computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
			const vector<vector<Point2f> >& imagePoints,
			const vector<Mat>& rvecs, const vector<Mat>& tvecs,
			const Mat& cameraMatrix , const Mat& distCoeffs,
			vector<float>& perViewErrors);
	static void calcBoardCornerPositions(cv::Size boardSize, float squareSize, vector<Point3f>& corners,
			Settings::Pattern patternType /*= Settings::CHESSBOARD*/);

	// Print camera parameters to the output file
	void saveCameraParams( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
			const vector<Mat>& rvecs, const vector<Mat>& tvecs,
			const vector<float>& reprojErrs, const vector<vector<Point2f> >& imagePoints,
			double totalAvgErr );
	void saveStereoCameraParams(Settings& s, Size& imageSize, Mat& R, Mat& T,
			Mat& E, Mat& F, float& reprojErrs, double totalAvgErr );

	void show_rect_images(
			cv::Mat& img_left_rect,
			cv::Mat& img_right_rect,
			cv::Mat& rectified_images);


};//End of class IntrinsicCalibration

static inline void read(const cv::FileNode& node, Settings& x, const Settings& default_value)
{
	if(node.empty())
		x = default_value;
	else
		x.read(node);
}

static inline void write(cv::FileStorage& fs, const String&, const Settings& s )
{
	s.write(fs);
}

#endif //__INTRINSICCALIBRATION_HPP_
