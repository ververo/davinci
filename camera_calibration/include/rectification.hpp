/**
 * @author 		Veronica Penza
 * @version 	1.0
 * @date 		7 Aug 2015
 * @brief image rectification
 * Class for images rectification using OpenCV Library
 */

#ifndef RECTIFICATION_HPP_
#define RECTIFICATION_HPP_


//include
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <sensor_msgs/image_encodings.h>

//OpenCV
#include <opencv2/opencv.hpp>
//define

//types
typedef struct{
	cv::Mat xL;
	cv::Mat yL;
	cv::Mat xR;
	cv::Mat yR;
	bool ready;
}Rmap;


//class
/** Rectification Class*/
class Rectification{

	//variables
private:
	Rmap rmaps;
	cv::Mat cameraMatrixL;
	cv::Mat distortionCoeffL;
	cv::Mat cameraMatrixR;
	cv::Mat distortionCoeffR;
	cv::Mat extrinsicR;
	cv::Mat extrinsicT;
	cv::Mat fundamentalMatrix;
	cv::Mat P1, P2;
	cv::Mat R1, R2;
	cv::Mat Q;
	cv::Size imageSize;

public:
	Rectification();
	~Rectification();

	//methods
	bool setParam(
			const std::string& fileName,
			bool camera_streaming);

	bool getRmap(Rmap& rmaps);

	bool process(
			const cv::Mat& imgL,
			const cv::Mat& imgR,
			cv::Mat& imgL_rect,
			cv::Mat& imgR_rect,
			cv::Mat& R1);

	cv::Mat* getQ();

};


#endif //RECTIFICATION_HPP_
