/*
 /*
 * setting.hpp
 *
 *  Created on: November 5, 2015
 *      Author: Veronica Penza
 */

#ifndef __SETTINGCALIBRATION_HPP_
#define __SETTINGCALIBRATION_HPP_

//ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sensor_msgs/fill_image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <std_msgs/Int32.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <sensor_msgs/Image.h>

//OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/cv_bridge.h>
#include "opencv/cxcore.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>


#include "enVisorsLib/common.hpp"

using namespace cv;
using namespace std;
using namespace sensor_msgs;
using namespace sensor_msgs::image_encodings;
using namespace message_filters;


class Settings
{
public:
	Settings();
	enum Pattern { NOT_EXISTING, CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID };
	enum InputType { INVALID, CAMERA, VIDEO_FILE, IMAGE_LIST, ROS }; 
	enum CalibrationType { INIT, LEFT, RIGHT, STEREO};

	void write(cv::FileStorage& fs) const;                        //Write serialization for this class
	void read(const cv::FileNode& node);
	void validate();
	cv::Mat nextImage();
	void nextImageStereo(cv::Mat& imL, cv::Mat& imR);
	static bool readStringList( const std::string& filename, std::vector<std::string>& l );
	// ros methods
	void stereo_images_callback(const ImageConstPtr& m, const ImageConstPtr& s);
	void left_image_callback(const ImageConstPtr& img);
	void right_image_callback(const ImageConstPtr& img);

	cv::Mat imgL;
	cv::Mat imgR;
	cv::Size imgSize;
	CalibrationType calib_type;

public:
	cv::Size boardSize;              // The size of the board -> Number of items by width and height
	Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
	float squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
	int nrFrames;                // The number of frames to use from the input for calibration
	int delay;                   // In case of a video input
	bool writePoints;            // Write detected feature points
	bool writeExtrinsics;        // Write extrinsic parameters
	bool flipVertical;           // Flip the captured images around the horizontal axis
	bool showUndistorsed;        // Show undistorted images after calibration
	std::string input;                // The input ->

	int cameraID;
	int cameraIDL;
	int cameraIDR;
	std::vector<std::string> imageList;
	std::size_t atImageList;
	cv::VideoCapture inputCapture;
	cv::VideoCapture inputCaptureL;
	cv::VideoCapture inputCaptureR;
	InputType inputType;
	bool goodInput;

	ros::NodeHandle n;
	message_filters::Subscriber<sensor_msgs::Image> *imgL_sub;
	message_filters::Subscriber<sensor_msgs::Image> *imgR_sub;
	typedef sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> MySyncPolicy;
	message_filters::Synchronizer<MySyncPolicy>* sync;

	image_transport::Subscriber imgL_itsub;
	image_transport::Subscriber imgR_itsub;

private:
	std::string patternToUse;

};

#endif //__SETTINGCALIBRATION_HPP_
