#include "extrinsic_calib/extrinsic_calibration.hpp"


ExtrinsicCalibration::ExtrinsicCalibration() :
n(),
it(n),
s()
{

	//	// init subscriber to image
	//	this->imgL_SUB = it.subscribe("/image/left/rgb", 1,
	//			&ExtrinsicCalibration::images_callback, this);

	// init variables
	this->counter_frames = 0;
	//this->max_n_frames = 5;
	this->calib_go = 0;

	this->inputSettingsFile = "calibrationConfig.xml";
	this->intrinsicsFile = "LeftCameraParameters.xml";
	this->s.calib_type = Settings::LEFT;//enum CalibrationType { INIT, LEFT, RIGHT, STEREO};

	this->chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;
	// fast check erroneously fails with high distortions like fisheye
	this->chessBoardFlags |= CALIB_CB_FAST_CHECK;

}


//void ExtrinsicCalibration::images_callback(
//		const sensor_msgs::Image::ConstPtr& l)
//{
//
//	fromImagetoMat(*l, &this->imgL, l->encoding);
//
//}

bool ExtrinsicCalibration::initParams()
{
	// Read the configuration and the intrinsics
	FileNode fn;
	FileStorage fs(this->inputSettingsFile, FileStorage::READ);
	if (!fs.isOpened())
	{
		std::cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << std::endl;
		return false;
	}
	fn = fs["Settings"];
	s.read(fn);
	//	fs["Settings"] >> s; //oss:questo nodo lavora solo su immagini ricevute da topic di ROS!!
	fs.release();
	// close Settings file


	cv::FileStorage fs2(this->intrinsicsFile, cv::FileStorage::READ);
	if (!fs2.isOpened())
	{
		std::cout << "Could not open the intrinsic calibration file: \"" << intrinsicsFile << "\"" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Loading: " << this->intrinsicsFile << std::endl;
	}
	fs2["M"]>> this->cameraMatrix;
	fs2["D"]>> this->distCoeffs;
	// fs2["camera_matrix"]>> this->cameraMatrix;
	// fs2["distortion_coefficients"]>> this->distCoeffs;
	fs2.release();
	// close intrinsics file


	if (!this->s.goodInput)
	{
		std::cout << "Invalid input detected. Application stopping. " << std::endl;
		return false;
	}
	return true;
}

void ExtrinsicCalibration::calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
		Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
{
	corners.clear();

	switch(patternType)
	{
	case Settings::CHESSBOARD:
	case Settings::CIRCLES_GRID:
		for( int i = 0; i < boardSize.height; ++i )
			for( int j = 0; j < boardSize.width; ++j )
				corners.push_back(Point3f(j*squareSize, i*squareSize, 0));
		break;

	case Settings::ASYMMETRIC_CIRCLES_GRID:
		for( int i = 0; i < boardSize.height; i++ )
			for( int j = 0; j < boardSize.width; j++ )
				corners.push_back(Point3f((2*j + i % 2)*squareSize, i*squareSize, 0));
		break;
	default:
		break;
	}
}
