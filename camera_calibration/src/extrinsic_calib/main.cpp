#include "extrinsic_calib/extrinsic_calibration.hpp"

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

#define ESC_KEY  27


int main(int argc, char* argv[])
{ 
	ros::init(argc, argv, "extrinsic_calibration");

	ExtrinsicCalibration ext;

	ros::Rate loop_rate(30);

	if(!ext.initParams())
		return false;

	startWindowThread();
	namedWindow("Extrinsic Calibration", CV_WINDOW_AUTOSIZE); //ERRORE1


	while (ros::ok())
	{
		if(!ext.s.imgL.empty())
		{
			ext.imgL = ext.s.nextImage();
			cv::imshow("Extrinsic Calibration", ext.s.imgL);
			ext.imageSize = ext.imgL.size();

			switch( ext.s.calibrationPattern ) // Find feature points on the input format
			{
			case Settings::CHESSBOARD:
				ext.found = findChessboardCorners( ext.imgL, ext.s.boardSize, ext.corners, ext.chessBoardFlags);
				break;
			case Settings::CIRCLES_GRID:
				ext.found = findCirclesGrid( ext.imgL, ext.s.boardSize, ext.corners );
				break;
			case Settings::ASYMMETRIC_CIRCLES_GRID:
				ext.found = findCirclesGrid( ext.imgL, ext.s.boardSize, ext.corners, CALIB_CB_ASYMMETRIC_GRID );
				break;
			default:
				ext.found = false;
				break;
			}

			// sub pixel corner
			if (ext.found)// If done with success,
			{
				// improve the found corners' coordinate accuracy for chessboard
				if(  ext.s.calibrationPattern == Settings::CHESSBOARD)
				{
					Mat viewGray;
					cvtColor(ext.imgL, viewGray, COLOR_BGR2GRAY);
					cornerSubPix( viewGray,  ext.corners, Size(11,11),
							Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
				}
				// Draw the corners.
				drawChessboardCorners( ext.imgL,  ext.s.boardSize, Mat( ext.corners), ext.found );


				// // //defining objectPoints (see "runCalib" method)
				std::vector<cv::Point3f> objectPoints;
				ext.calcBoardCornerPositions(ext.s.boardSize, ext.s.squareSize, objectPoints, ext.s.calibrationPattern);
				objectPoints.resize(ext.corners.size());
				//The object points are 3D, but since they are in a pattern coordinate system, then, if the rig is planar, it may make sense to put the model to a XY coordinate plane so that Z-coordinate of each input object point is 0.

				//computing extrinsics
				solvePnP(objectPoints, ext.corners, ext.cameraMatrix, ext.distCoeffs, ext.rvec, ext.tvec, false );


				cv::Mat rvec_r;
				Rodrigues(ext.rvec, rvec_r);



				//publishing on tf the extrinsic transformation
				tf::Transform cam_wrt_world;
				cam_wrt_world.setOrigin(tf::Vector3(ext.tvec.at<double>(0)/1000,ext.tvec.at<double>(1)/1000,ext.tvec.at<double>(2)/1000 ));
				cam_wrt_world.setBasis(tf::Matrix3x3(rvec_r.at<double>(0,0),rvec_r.at<double>(1,0),rvec_r.at<double>(2,0),
						rvec_r.at<double>(0,1),rvec_r.at<double>(1,1),rvec_r.at<double>(2,1),
						rvec_r.at<double>(0,2),rvec_r.at<double>(1,2),rvec_r.at<double>(2,2)));

				//if(!ext.rvec.empty() && !ext.tvec.empty())
				ext.broadcaster.sendTransform(tf::StampedTransform(cam_wrt_world, ros::Time::now(), "/base_cam", "/chess"));

				// //keep on calibrating
				// ext.calib_go=0;

				//}//ext.calib_go==1

			}//ext.found

	
		}
		ros::spinOnce();
		loop_rate.sleep();

	}//ros ok

	return 0;
}



