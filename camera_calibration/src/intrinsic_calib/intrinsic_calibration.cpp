/*
 /*
 * stereo_calibration.cpp
 *
 *  Created on: November 5, 2015
 *      Author: Veronica Penza
 */

#include "intrinsic_calib/intrinsic_calibration.hpp"

IntrinsicCalibration::IntrinsicCalibration()
{
	capturing_step = false;
	stereocalibration_sep = false;
}

void IntrinsicCalibration::help()
{
	cout <<  "This is a camera calibration sample." << endl
			<<  "Usage: calibration configurationFile"  << endl
			<<  "Near the sample file you'll find the configuration file, which has detailed help of "
			"how to edit it.  It may be any OpenCV supported file format XML/YAML." << endl;
}


//! [compute_errors]
double IntrinsicCalibration::computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
		const vector<vector<Point2f> >& imagePoints,
		const vector<Mat>& rvecs, const vector<Mat>& tvecs,
		const Mat& cameraMatrix , const Mat& distCoeffs,
		vector<float>& perViewErrors)
{
	vector<Point2f> imagePoints2;
	size_t totalPoints = 0;
	double totalErr = 0, err;
	perViewErrors.resize(objectPoints.size());

	for(size_t i = 0; i < objectPoints.size(); ++i )
	{

		projectPoints(objectPoints[i], rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);

		err = norm(imagePoints[i], imagePoints2, NORM_L2);

		size_t n = objectPoints[i].size();
		perViewErrors[i] = (float)sqrt(err*err/n);
		totalErr        += err*err;
		totalPoints     += n;
	}

	return std::sqrt(totalErr/totalPoints);
}
//! [compute_errors]
//! [board_corners]
void IntrinsicCalibration::calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
		Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
{
	corners.clear();

	switch(patternType)
	{
	case Settings::CHESSBOARD:
	case Settings::CIRCLES_GRID:
		for( int i = 0; i < boardSize.height; ++i )
			for( int j = 0; j < boardSize.width; ++j )
				corners.push_back(Point3f(j*squareSize, i*squareSize, 0));
		break;

	case Settings::ASYMMETRIC_CIRCLES_GRID:
		for( int i = 0; i < boardSize.height; i++ )
			for( int j = 0; j < boardSize.width; j++ )
				corners.push_back(Point3f((2*j + i % 2)*squareSize, i*squareSize, 0));
		break;
	default:
		break;
	}
}
//! [board_corners]
bool IntrinsicCalibration::runCalibration( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
		vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs,
		vector<float>& reprojErrs,  double& totalAvgErr)
{

	//! [fixed_aspect]
	cameraMatrix = Mat::eye(3, 3, CV_32F);
	//! [fixed_aspect]
	distCoeffs = Mat::zeros(8, 1, CV_32F);


	vector<vector<Point3f> > objectPoints(1);
	calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

	objectPoints.resize(imagePoints.size(),objectPoints[0]);

	//Find intrinsic and extrinsic camera parameters
	double rms;


	//first computing
	calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
			CALIB_FIX_K3);
	//computing only intrinsic
	calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
			CALIB_FIX_K1|CALIB_FIX_K2|CALIB_FIX_K3|CALIB_USE_INTRINSIC_GUESS);
	//computing only focal lenght
	calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
			CALIB_FIX_K1|CALIB_FIX_K2|CALIB_FIX_K3|CALIB_FIX_PRINCIPAL_POINT|CALIB_USE_INTRINSIC_GUESS);
	//computing only distortion
	calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
			CALIB_FIX_K3|CALIB_FIX_PRINCIPAL_POINT|CALIB_FIX_FOCAL_LENGTH);
	//final recomputing
	rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs,
			CALIB_FIX_K3|CALIB_USE_INTRINSIC_GUESS);


	cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

	totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix,
			distCoeffs, reprojErrs);

	return ok;
}


bool IntrinsicCalibration::runStereoCalibration( Settings& s, Size& imageSize,
		Mat& cameraMatrixL, Mat& distCoeffsL,
		vector<vector<Point2f> > imagePointsL,
		Mat& cameraMatrixR, Mat& distCoeffsR,vector<vector<Point2f> > imagePointsR,
		cv::Mat& R, cv::Mat& T, cv::Mat& E, cv::Mat& F,
		float& reprojErrs,  double& totalAvgErr)
{
	vector<vector<Point3f> > objectPoints(1);
	calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

	objectPoints.resize(imagePointsL.size(),objectPoints[0]);


	//Find intrinsic and extrinsic camera parameters
	double rms_stereo;
	if(stereocalibration_sep)
	{
		rms_stereo = stereoCalibrate(objectPoints, imagePointsL, imagePointsR,
				cameraMatrixL, distCoeffsL,
				cameraMatrixR, distCoeffsR, imageSize,
				/*output*/
				R, T, E, F,
				/*cv::TermCriteria(cv::TermCriteria::MAX_ITER + cv::TermCriteria::EPS, 300, 1e-5)*/
				CV_CALIB_FIX_INTRINSIC);
	}
	else
	{
		rms_stereo = stereoCalibrate(objectPoints, imagePointsL, imagePointsR,
				cameraMatrixL, distCoeffsL,
				cameraMatrixR, distCoeffsR, imageSize,
				/*output*/
				R, T, E, F /*,
				cv::TermCriteria(cv::TermCriteria::MAX_ITER + cv::TermCriteria::EPS, 300, 1e-5)*/ );
	}

	//TermCriteria::COUNT+TermCriteria::EPS, 30, 1e-6)
	cout << "Re-projection error reported by calibrateCamera: "<< rms_stereo << endl;

	//	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

	//	totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, cameraMatrix,
	//			distCoeffs, reprojErrs);

	return true;
}

// Print camera parameters to the output file
void IntrinsicCalibration::saveCameraParams( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs,
		const vector<Mat>& rvecs, const vector<Mat>& tvecs,
		const vector<float>& reprojErrs, const vector<vector<Point2f> >& imagePoints,
		double totalAvgErr )
{
	std::string fileName;
	switch (s.calib_type)
	{
	case Settings::LEFT:
		fileName = "LEFTCalibrationParams.xml";
		break;
	case Settings::RIGHT:
		fileName = "RIGHTCalibrationParams.xml";
		break;
	}

	FileStorage fs(fileName, FileStorage::WRITE );

	time_t tm;
	time( &tm );
	struct tm *t2 = localtime( &tm );
	char buf[1024];
	strftime( buf, sizeof(buf), "%c", t2 );

	fs << "calibration_time" << buf;

	if( !rvecs.empty() || !reprojErrs.empty() )
		fs << "nr_of_frames" << (int)std::max(rvecs.size(), reprojErrs.size());

	fs << "image_width" << imageSize.width;
	fs << "image_height" << imageSize.height;
	fs << "board_width" << s.boardSize.width;
	fs << "board_height" << s.boardSize.height;
	fs << "square_size" << s.squareSize;
	fs << "camera_matrix" << cameraMatrix;
	fs << "distortion_coefficients" << distCoeffs;
	fs << "avg_reprojection_error" << totalAvgErr;

	std::cout << "square size: " << s.squareSize << std::endl;


	if (s.writeExtrinsics && !reprojErrs.empty())
		fs << "per_view_reprojection_errors" << Mat(reprojErrs);

	if(s.writeExtrinsics && !rvecs.empty() && !tvecs.empty() )
	{
		CV_Assert(rvecs[0].type() == tvecs[0].type());
		Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
		for( size_t i = 0; i < rvecs.size(); i++ )
		{
			Mat r = bigmat(Range(int(i), int(i+1)), Range(0,3));
			Mat t = bigmat(Range(int(i), int(i+1)), Range(3,6));

			CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
			CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
			//*.t() is MatExpr (not Mat) so we can use assignment operator
			r = rvecs[i].t();
			t = tvecs[i].t();
		}
		//cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
		fs << "extrinsic_parameters" << bigmat;
	}

	if(s.writePoints && !imagePoints.empty() )
	{
		Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
		for( size_t i = 0; i < imagePoints.size(); i++ )
		{
			Mat r = imagePtMat.row(int(i)).reshape(2, imagePtMat.cols);
			Mat imgpti(imagePoints[i]);
			imgpti.copyTo(r);
		}
		fs << "image_points" << imagePtMat;
	}
}


void IntrinsicCalibration::saveStereoCameraParams( Settings& s, Size& imageSize, Mat& R, Mat& T,
		Mat& E, Mat& F, float& reprojErrs, double totalAvgErr )
{

	std::string fileName;

	fileName = "STEREOCalibrationParams.xml";
	FileStorage fs(fileName, FileStorage::WRITE );

	time_t tm;
	time( &tm );
	struct tm *t2 = localtime( &tm );
	char buf[1024];
	strftime( buf, sizeof(buf), "%c", t2 );

	fs << "calibration_time" << buf;
	fs << "image_width" << imageSize.width;
	fs << "image_height" << imageSize.height;
	fs << "board_width" << s.boardSize.width;
	fs << "board_height" << s.boardSize.height;
	fs << "square_size" << s.squareSize;
	fs << "R" << R;
	fs << "T" << T;
	fs << "E" << E;
	fs << "F" << F;
	fs << "avg_reprojection_error" << totalAvgErr;

}


//! [run_and_save]
bool IntrinsicCalibration::runCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrix, Mat& distCoeffs,
		vector<vector<Point2f> > imagePoints)
{
	vector<Mat> rvecs, tvecs;
	vector<float> reprojErrs;
	double totalAvgErr = 0;

	bool ok = runCalibration(s, imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs, reprojErrs,
			totalAvgErr);
	cout << (ok ? "Calibration succeeded" : "Calibration failed")
        						 << ". avg re projection error = " << totalAvgErr << endl;

	if (ok)
	{
		if(s.calib_type == Settings::LEFT)
		{
			saveParamsCamera( //tag in calibrationConfig.xml not used???
					"LeftCameraParameters.xml",
					cameraMatrix,
					distCoeffs);
			std::cout << "Saving file in: " << "LeftCameraParameters.xml" << std::endl;
		}
		else if(s.calib_type == Settings::RIGHT)
		{
			saveParamsCamera(
					"RightCameraParameters.xml",
					cameraMatrix,
					distCoeffs);
			std::cout << "Saving file in: " << "RightCameraParameters.xml" << std::endl;
		}

	}

	//        saveCameraParams(s, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, reprojErrs, imagePoints,
	//                         totalAvgErr);
	return ok;
}

bool IntrinsicCalibration::runStereoCalibrationAndSave(Settings& s, Size imageSize,
		Mat&  cameraMatrixL, Mat& distCoeffsL,
		vector<vector<Point2f> > imagePointsL,
		Mat&  cameraMatrixR, Mat& distCoeffsR,
		vector<vector<Point2f> > imagePointsR)
{
	cv::Mat R, T, E, F;
	float reprojErrs;
	double totalAvgErr = 0;

//leggo la calibrazione delle singole camere e la uso per fare la calibrazione stereo
	readParamsCamera(
			"LeftCameraParameters.xml",
			cameraMatrixL,
			distCoeffsL);
	readParamsCamera(
			"RightCameraParameters.xml",
			cameraMatrixR,
			distCoeffsR);


	bool ok = runStereoCalibration(s, imageSize, cameraMatrixL, distCoeffsL,
			imagePointsL,  cameraMatrixR, distCoeffsR,
			imagePointsR, R, T, E, F,reprojErrs,
			totalAvgErr);
	cout << (ok ? "Calibration succeeded" : "Calibration failed")
        										 << ". avg re projection error = " << totalAvgErr << endl;


	std::cout << "Opening calibration parameters completed........................." <<std::endl;
	std::cout << "M_l " << cameraMatrixL << std::endl;
	std::cout << "M_r " << cameraMatrixR << std::endl;
	std::cout << "D_l " << distCoeffsL << std::endl;
	std::cout << "D_r " << distCoeffsR << std::endl;
	std::cout << "Opening calibration parameters completed..............................." << std::endl;


	if (ok)
		saveStereoParamsCamera(
				"StereoCameraParameters.xml",
				cameraMatrixL,
				distCoeffsL,
				cameraMatrixR,
				distCoeffsR,
				R, T, E, F);
	//		saveStereoCameraParams(s, imageSize, R, T, E, F, reprojErrs,totalAvgErr);
	std::cout << "Saving file in: " <<  "StereoCameraParameters.xml" << std::endl;
	return ok;
}

void IntrinsicCalibration::show_rect_images(
		cv::Mat& img_left_rect,
		cv::Mat& img_right_rect,
		cv::Mat& rectified_images)
{

	if(!img_left_rect.empty() && !img_right_rect.empty())
	{

		rectified_images.create(img_left_rect.rows, img_left_rect.cols*2, CV_8UC3);

		for(int i = 0; i < img_left_rect.rows; i++)
		{
			for(int j = 0; j < img_left_rect.cols; j++)
			{
				rectified_images.at<cv::Vec3b>(i,j) = img_left_rect.at<cv::Vec3b>(i,j);
			}
			for(int k =  img_left_rect.cols; k <  img_left_rect.cols*2; k++)
			{
				rectified_images.at<cv::Vec3b>(i,k) = img_right_rect.at<cv::Vec3b>(i, k - img_left_rect.cols);
			}
		}

		Point pt1, pt2;
		for (int i=0; i< rectified_images.rows; i+=16){

			pt1.x = 0;
			pt1.y = i;
			pt2.x = rectified_images.cols-1;
			pt2.y = i;
			//if(i%7==0)
			cv::line(rectified_images, pt1, pt2, CV_RGB(0, 255, 0));

		}

	}

}
//! [run_and_save]

