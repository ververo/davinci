/*
 * main.cpp
 *
 *  Created on: Nov 5, 2015
 *      Author: veronica
 */

#include "intrinsic_calib/intrinsic_calibration.hpp"

#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

using namespace cv;
using namespace std;


int main(int argc, char* argv[])
{

	ros::init(argc, argv, "stereo_calibration");

	// Class definition
	IntrinsicCalibration CamCalib;
	Settings s;
	Rectification rect;
	char key;
	bool next = false;

	ros::Rate loop_rate(30);

	s.calib_type = Settings::INIT;

	// check arguments 
	if(argc != 2)
	{
		cout << "[ERROR] Wrong number of arguments" << endl;
		return 0;
	}
	else
	{
		if(strcmp(argv[1], "left") == 0)
		{
			s.calib_type = Settings::LEFT;
		}
		else if(strcmp(argv[1], "right") == 0)
		{
			s.calib_type = Settings::RIGHT;
		}
		else if(strcmp(argv[1], "stereo") == 0)
		{
			s.calib_type = Settings::STEREO;
		}
		else
		{
			std::cout << "[ERROR] Wrong number of arguments" << std::endl;
			return 0;
		}
	}


	const std::string inputSettingsFile = std::string("calibrationConfig.xml");

	// Read the settings
	FileStorage fs(inputSettingsFile, FileStorage::READ);
	if (!fs.isOpened())
	{
		std::cout << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << std::endl;
		return -1;
	}

	fs["Settings"] >> s;
	fs.release();
	// close Settings file

	//! [file_read]

	startWindowThread(); //errore: (Left Calibration:9569): GLib-GObject-CRITICAL **: g_object_unref: assertion 'G_IS_OBJECT (object)' failed
    switch (s.calib_type)
	{
	case Settings::LEFT:
		namedWindow("Left Calibration", CV_WINDOW_AUTOSIZE);
		break;
	case Settings::RIGHT:
		namedWindow("Right Calibration", CV_WINDOW_AUTOSIZE);
		break;
	case Settings::STEREO:
		namedWindow("Left Calibration", CV_WINDOW_AUTOSIZE);
		namedWindow("Right Calibration", CV_WINDOW_AUTOSIZE);
		break;
	}
    
   
	if (!s.goodInput)
	{
		std::cout << "Invalid input detected. Application stopping. " << std::endl;
		return -1;
	}

	std::vector<std::vector<Point2f> > imagePoints; //corners
	std::vector<std::vector<Point2f> > imagePointsL;
	std::vector<std::vector<Point2f> > imagePointsR;
	cv::Mat cameraMatrix, distCoeffs;
	cv::Mat cameraMatrixL, distCoeffsL;
	cv::Mat cameraMatrixR, distCoeffsR;
	cv::Size imageSize;
	int mode = s.inputType == Settings::IMAGE_LIST ? CamCalib.CAPTURING : CamCalib.DETECTION; 	clock_t prevTimestamp = 0;
	const Scalar RED(0,0,255), GREEN(0,255,0);
	const char ESC_KEY = 27;


	while (ros::ok())
	{
		cv::Mat view;
		cv::Mat viewL;
		cv::Mat viewR;
		bool blinkOutput = false;
		bool step = false;

		//next image
		switch (s.calib_type)
		{
		case Settings::LEFT:
			view = s.nextImage(); //se input=ROS: legge l'attuale imgL (dove imgL viene riempita da una left_image_callback ad ogni pubblicazione del nodo camera_to_ros su /image/left/rgb)
			break;
		case Settings::RIGHT:
			view = s.nextImage();
			break;
		case Settings::STEREO:
			s.nextImageStereo(viewL, viewR);
			break;
		}

		if(s.calib_type == Settings::STEREO)
		{

			//-----  If no more image, or got enough, then stop calibration and show result -------------
			if( mode == CamCalib.CAPTURING &&
					imagePointsL.size() >= (size_t)s.nrFrames &&
					imagePointsR.size() >= (size_t)s.nrFrames
			)
			{
				if( CamCalib.runStereoCalibrationAndSave(s, imageSize,  cameraMatrixL, distCoeffsL, imagePointsL,
						cameraMatrixR, distCoeffsR, imagePointsR))
					mode = CamCalib.CALIBRATED;

				else
					mode = CamCalib.DETECTION;

			}
		}
		else
		{
			//-----  If no more image, or got enough, then stop calibration and show result -------------
			if( mode == CamCalib.CAPTURING && imagePoints.size() >= (size_t)s.nrFrames)
			{


				if( CamCalib.runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints))
					mode = CamCalib.CALIBRATED;
				else
					mode = CamCalib.DETECTION;

			}
			step = false;
		}

		if(s.calib_type == Settings::STEREO)
		{

			if(viewL.empty() || viewR.empty())          // If there are no more images stop the loop
			{

				if(s.inputType == s.ROS)
				{

					ros::spinOnce();
					loop_rate.sleep();
					continue;
				}
				else
				{

					// if calibration threshold was not reached yet, calibrate now
					if( mode != CamCalib.CALIBRATED && !imagePointsL.empty()
							&& !imagePointsR.empty())
						CamCalib.runStereoCalibrationAndSave(s, imageSize,  cameraMatrixL, distCoeffsL, imagePointsL,
								cameraMatrixR, distCoeffsR, imagePointsR);
					break;
				}
			}
		}
		else if(view.empty())          // If there are no more images stop the loop
		{
			if(s.inputType == s.ROS)
			{
				ros::spinOnce();
				loop_rate.sleep();
				continue;
			}
			else
			{

				std::cout << CamCalib.CALIBRATED << std::endl;
				std::cout << imagePoints.empty() << std::endl;
				// if calibration threshold was not reached yet, calibrate now
				if( mode != CamCalib.CALIBRATED && !imagePoints.empty() ){

					CamCalib.runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints);
				}

				break;
			}

		}



		//! [get_input]

		if(s.calib_type == Settings::STEREO)
			imageSize = viewL.size();
		else
			imageSize = view.size();
		if( s.flipVertical )    flip( view, view, 0 );

		vector<Point2f> pointBuf;
		vector<Point2f> pointBufL;
		vector<Point2f> pointBufR;

		bool found;
		bool foundL;
		bool foundR;

		// init flag
		int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;
		// fast check erroneously fails with high distortions like fisheye
		chessBoardFlags |= CALIB_CB_FAST_CHECK;


		//! [find_pattern]
		if(s.calib_type == Settings::STEREO)
		{

			switch( s.calibrationPattern ) // Find feature points on the input format
			{
			case Settings::CHESSBOARD:
				foundL = findChessboardCorners( viewL, s.boardSize, pointBufL, chessBoardFlags);
				foundR = findChessboardCorners( viewR, s.boardSize, pointBufR, chessBoardFlags);
				break;
			case Settings::CIRCLES_GRID:
				foundL = findCirclesGrid( viewL, s.boardSize, pointBufL );
				foundR = findCirclesGrid( viewR, s.boardSize, pointBufR );
				break;
			case Settings::ASYMMETRIC_CIRCLES_GRID:
				foundL = findCirclesGrid( viewL, s.boardSize, pointBufL, CALIB_CB_ASYMMETRIC_GRID );
				foundR = findCirclesGrid( viewR, s.boardSize, pointBufR, CALIB_CB_ASYMMETRIC_GRID );
				break;
			default:
				foundL = false;
				foundR = false;
				break;
			}
		}
		else
		{

			switch( s.calibrationPattern ) // Find feature points on the input format
			{
			case Settings::CHESSBOARD:
				found = findChessboardCorners( view, s.boardSize, pointBuf, chessBoardFlags);
				break;
			case Settings::CIRCLES_GRID:
				found = findCirclesGrid( view, s.boardSize, pointBuf );
				break;
			case Settings::ASYMMETRIC_CIRCLES_GRID:
				found = findCirclesGrid( view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID );
				break;
			default:
				found = false;
				break;
			}
		}
		//! [pattern_found]



		// sub pixel corner
		if(s.calib_type == Settings::STEREO)
		{
			if ( foundL && foundR)// If done with success,
			{

				// improve the found corners' coordinate accuracy for chessboard
				if( s.calibrationPattern == Settings::CHESSBOARD)
				{
					Mat viewGrayL;
					Mat viewGrayR;
					//LEFT
					cvtColor(viewL, viewGrayL, COLOR_BGR2GRAY);
					cornerSubPix( viewGrayL, pointBufL, Size(11,11),
							Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
					//RIGHT
					cvtColor(viewR, viewGrayR, COLOR_BGR2GRAY);
					cornerSubPix( viewGrayR, pointBufR, Size(11,11),
							Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
				}

				key = (char)waitKey(s.inputCaptureL.isOpened() ? 50 : s.delay);
				step = false;
				if(key == 'h')
				{
					step = true;
				}

				//        		if( mode == CamCalib.CAPTURING &&  // For camera only take new samples after delay time
				//        				(!s.inputCaptureL.isOpened() || !s.inputCaptureR.isOpened() || clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC) )
				//        		{
				if( mode == CamCalib.CAPTURING &&  step )
				{

					imagePointsL.push_back(pointBufL);
					imagePointsR.push_back(pointBufR);
					prevTimestamp = clock();
					blinkOutput = s.inputCaptureL.isOpened();
				}

				// Draw the corners.
				drawChessboardCorners( viewL, s.boardSize, Mat(pointBufL), foundL );
				drawChessboardCorners( viewR, s.boardSize, Mat(pointBufR), foundR );
			}
			//! [pattern_found]
		}
		else
		{
			if ( found)// If done with success,
			{
				// improve the found corners' coordinate accuracy for chessboard
				if( s.calibrationPattern == Settings::CHESSBOARD)
				{
					Mat viewGray;
					cvtColor(view, viewGray, COLOR_BGR2GRAY);
					cornerSubPix( viewGray, pointBuf, Size(11,11),
							Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
				}

				key = (char)waitKey(s.inputCaptureL.isOpened() ? 50 : s.delay);
				step = false;
				if(key == 'h')
				{
					step = true;
				}

				if(CamCalib.capturing_step)
				{
					if(mode == CamCalib.CAPTURING &&  step )
					{
						imagePoints.push_back(pointBuf);
						prevTimestamp = clock();
						blinkOutput = s.inputCapture.isOpened();
					}
				}
				else		{
					if( mode == CamCalib.CAPTURING &&  // For camera only take new samples after delay time
							(!s.inputCapture.isOpened() || clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC) )
					{
						imagePoints.push_back(pointBuf);
						prevTimestamp = clock();
						blinkOutput = s.inputCapture.isOpened();
					}

				}

				// Draw the corners.
				drawChessboardCorners( view, s.boardSize, Mat(pointBuf), found );
			}
			//! [pattern_found]
		}

		//----------------------------- Output Text ------------------------------------------------
		//! [output_text]
		string msg = (mode == CamCalib.CAPTURING) ? "100/100" :
				((mode == CamCalib.CALIBRATED) ? "Calibrated" : "Press 'g' to start");
		int baseLine = 0;
		Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);

		Point textOrigin;

		if(s.calib_type == Settings::STEREO)
		{

			textOrigin = Point(viewL.cols - 2*textSize.width - 10, viewL.rows - 2*baseLine - 10);
			if(s.showUndistorsed)
				msg = format( "%d/%d Undist", (int)imagePointsL.size(), s.nrFrames );
			else
				msg = format( "%d/%d", (int)imagePointsL.size(), s.nrFrames );

			putText( viewL, msg, textOrigin, 1, 1, mode == CamCalib.CALIBRATED ?  GREEN : RED);
		}
		else
		{

			textOrigin= Point(view.cols - 2*textSize.width - 10, view.rows - 2*baseLine - 10);
			if(s.showUndistorsed)
				msg = format( "%d/%d Undist", (int)imagePoints.size(), s.nrFrames );
			else
				msg = format( "%d/%d", (int)imagePoints.size(), s.nrFrames );

			putText( view, msg, textOrigin, 1, 1, mode == CamCalib.CALIBRATED ?  GREEN : RED);
		}


		if( blinkOutput )
			bitwise_not(view, view);
		//! [output_text]
		//------------------------- Video capture  output  undistorted ------------------------------
		//! [output_undistorted]
		if( mode == CamCalib.CALIBRATED && s.showUndistorsed )
		{

			if(s.calib_type == Settings::STEREO)
			{
				// do rectification

				cv::Mat tempL;
				cv::Mat tempR;
				cv::Mat imL_rect;
				cv::Mat imR_rect;
				cv::Mat R1;
				cv::Mat imgs_rect;
				bool rectified = false;

				viewL.copyTo(tempL);
				viewR.copyTo(tempR);

				std::cout <<tempL.cols << std::endl;
				std::cout << tempL.rows << std::endl;

				if(!rectified){
				rect.setParam(
						"StereoCameraParameters.xml",
						false);
				rectified = true;
				}

				rect.process(
						tempL,
						tempR,
						imL_rect,
						imR_rect,
						R1);

				CamCalib.show_rect_images(
						imL_rect,
						imR_rect,
						imgs_rect);

				// save R1
				cv::FileStorage fs( "R1.xml",
						cv::FileStorage::WRITE);

				fs << "R1" << R1;
				fs.release();

				//show rectified images
				cv::namedWindow("Images Rectified", CV_WINDOW_NORMAL);
				cv::imshow("Images Rectified", imgs_rect);


				// save rectification transformation
			}
			else
			{
				Mat temp = view.clone();
				undistort(temp, view, cameraMatrix, distCoeffs);
			}
		}
		//! [output_undistorted]
		//------------------------------ Show image and check for input commands -------------------
		//! [await_input]

		switch (s.calib_type)
		{
		case Settings::LEFT:
			imshow("Left Calibration", view);
			break;
		case Settings::RIGHT:
			imshow("Right Calibration", view);
			break;
		case Settings::STEREO:
			imshow("Left Calibration", viewL);
			imshow("Right Calibration", viewR);
			break;
		}



		if(s.calib_type == Settings::STEREO)
		{
			key = (char)waitKey(s.inputCaptureL.isOpened() ? 50 : s.delay);
		}
		else
		{
			key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);
		}


		if( key  == ESC_KEY )
			break;
		if( key == 'u' && mode == CamCalib.CALIBRATED )
			s.showUndistorsed = !s.showUndistorsed;

		if(s.calib_type == Settings::STEREO)
		{
			if( (s.inputCaptureL.isOpened() || s.inputCaptureR.isOpened() || s.inputType == s.ROS) && key == 'g' )
			{
				mode = CamCalib.CAPTURING;
				imagePointsL.clear();
				imagePointsR.clear();
			}
			if( (s.inputCaptureL.isOpened() || s.inputCaptureR.isOpened() || s.inputType == s.ROS) && key == 'f' )
			{
				mode = CamCalib.CAPTURING;
				CamCalib.capturing_step = true;
				imagePointsL.clear();
				imagePointsR.clear();
			}
		}
		else
		{
			if( (s.inputCapture.isOpened() || s.inputType == s.ROS) && key == 'g' )
			{
				mode = CamCalib.CAPTURING;
				imagePoints.clear();
			}
			if( (s.inputCapture.isOpened() || s.inputType == s.ROS) && key == 'f' )
			{
				mode = CamCalib.CAPTURING;
				CamCalib.capturing_step = true;
				imagePoints.clear();
			}
		}

		//! [await_input]


		ros::spinOnce();
		loop_rate.sleep();

	}

	// -----------------------Show the undistorted image for the image list ------------------------
	//! [show_results]
	if( s.inputType == Settings::IMAGE_LIST && s.showUndistorsed )
	{
		Mat view, viewL, viewR, rview, rviewL, rviewR, map1, map2, map1L,map2L, map1R, map2R;


		if(s.calib_type == Settings::STEREO)
		{
			initUndistortRectifyMap(
					cameraMatrixL, distCoeffsL, Mat(),
					getOptimalNewCameraMatrix(cameraMatrixL, distCoeffsL, imageSize, 1, imageSize, 0), imageSize,
					CV_16SC2, map1L, map2L);
			initUndistortRectifyMap(
					cameraMatrixR, distCoeffsR, Mat(),
					getOptimalNewCameraMatrix(cameraMatrixR, distCoeffsR, imageSize, 1, imageSize, 0), imageSize,
					CV_16SC2, map1R, map2R);
		}
		else{
			initUndistortRectifyMap(
					cameraMatrix, distCoeffs, Mat(),
					getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0), imageSize,
					CV_16SC2, map1, map2);
		}


		if(s.calib_type == Settings::STEREO)
		{
			for(size_t i = 0; i < s.imageList.size()/2; i++ )
			{
				viewL = imread(s.imageList[i], 1);
				if(viewL.empty())
					continue;
				remap(viewL, rviewL, map1L, map2L, INTER_LINEAR);
				imshow("Left Calibration", rviewL);
				char c = (char)waitKey();
				if( c  == ESC_KEY || c == 'q' || c == 'Q' )
					break;
			}
			for(size_t i = s.imageList.size()/2; i < s.imageList.size(); i++ )
			{
				viewR = imread(s.imageList[i], 1);
				if(viewR.empty())
					continue;
				remap(viewR, rviewR, map1R, map2R, INTER_LINEAR);
				imshow("Right Calibration", rviewR);
				char c = (char)waitKey();
				if( c  == ESC_KEY || c == 'q' || c == 'Q' )
					break;
			}
		}
		else
		{
			for(size_t i = 0; i < s.imageList.size(); i++ )
			{
				view = imread(s.imageList[i], 1);
				if(view.empty())
					continue;
				remap(view, rview, map1, map2, INTER_LINEAR);
				if(s.calib_type == Settings::LEFT)
				{
					imshow("Left Calibration", rview);
				}
				else if(s.calib_type == Settings::RIGHT)
				{
					imshow("Right Calibration", rview);
				}

				char c = (char)waitKey();
				if( c  == ESC_KEY || c == 'q' || c == 'Q' )
					break;
			}
		}

	}
	//! [show_results]

	return 0;
}



