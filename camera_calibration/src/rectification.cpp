#include "enVisorsLib/rectification.hpp"

Rectification::Rectification()
{
    rmaps.ready = false;
}

Rectification::~Rectification()
{

}

//methods
bool Rectification::setParam(
        const std::string& fileName,
        bool camera_streaming)
{
    //reading intrinsic parameters
    cv::FileStorage fs;
    fs.open(fileName, cv::FileStorage::READ);

    if(!fs.isOpened())
    {
        std::cout << "Failed to open " << fileName << std::endl;
        return false;
    }

    fs["M_l"] >> cameraMatrixL;
    fs["M_r"] >> cameraMatrixR;
    fs["D_l"] >> distortionCoeffL;
    fs["D_r"] >> distortionCoeffR;
    fs["R"] >> extrinsicR;
    fs["T"] >> extrinsicT;
    fs["F"] >> fundamentalMatrix;

    std::cout << "Opening calibration parameters completed........................." <<std::endl;
    std::cout << "M_l " << cameraMatrixL << std::endl;
    std::cout << "M_r " << cameraMatrixR << std::endl;
    std::cout << "D_l " << distortionCoeffL << std::endl;
    std::cout << "D_r " << distortionCoeffR << std::endl;
    std::cout << "R " << extrinsicR << std::endl;
    std::cout << "T " << extrinsicT << std::endl;

    std::cout << "Opening calibration parameters completed..............................." << std::endl;

    return true;
}

bool Rectification::getRmap(Rmap& rmaps)
{
    rmaps = this->rmaps;

    return true;
}

bool Rectification::process(
        const cv::Mat& imgL,
        const cv::Mat& imgR,
        cv::Mat& imgL_rect,
        cv::Mat& imgR_rect,
        cv::Mat& R1)
{
    if(imgL.cols != imgR.cols || imgL.rows != imgR.rows)
        return false;
    this->imageSize.width = imgL.cols;
    this->imageSize.height = imgL.rows;
//    std::cout << cameraMatrixL << std::endl;
//    std::cout << cameraMatrixR << std::endl;
//    std::cout << distortionCoeffL << std::endl;
//    std::cout << distortionCoeffR << std::endl;
    stereoRectify(
            this->cameraMatrixL, this->distortionCoeffL,
            this->cameraMatrixR, this->distortionCoeffR,
            this->imageSize,
            this->extrinsicR,this->extrinsicT,
            this->R1,this->R2,this->P1,this->P2,
            this->Q,
            -1 /*cv::CALIB_ZERO_DISPARITY*/,0,
            this->imageSize );
    //	bool isVerticalStereo = fabs(P2.at<double>(1, 3)) > fabs(P2.at<double>(0, 3));

    // COMPUTE AND DISPLAY RECTIFICATION
    this->R1.convertTo(R1,CV_32F);
    this-> P1.convertTo(P1,CV_32F);
    this-> R2.convertTo(R2,CV_32F);
    this-> P2.convertTo(P2,CV_32F);

    //Precompute maps for cv::remap()
    initUndistortRectifyMap(cameraMatrixL,distortionCoeffL,
            this->R1, this->P1, this->imageSize, CV_32FC1, rmaps.xL, rmaps.yL);
    initUndistortRectifyMap(cameraMatrixR, distortionCoeffR,
            this->R2, this->P2, this->imageSize, CV_32FC1, rmaps.xR, rmaps.yR);

    remap(imgL, imgL_rect, rmaps.xL, rmaps.yL, cv::INTER_LINEAR);
    remap(imgR, imgR_rect, rmaps.xR, rmaps.yR, cv::INTER_LINEAR);
    rmaps.ready = true;
    R1 = this->R1;
    return true;
}

cv::Mat* Rectification::getQ(){

    return &this->Q;
}
