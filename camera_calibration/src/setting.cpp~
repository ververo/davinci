/*
 /*
 * setting.cpp
 *
 *  Created on: November 5, 2015
 *      Author: Veronica Penza
 */

#include"setting.hpp"




Settings::Settings():goodInput(false)
{
}

void Settings::stereo_images_callback(const ImageConstPtr& l, const ImageConstPtr& r)
{
	imgSize.height = l->height;
	imgSize.width = l->width;

	int frame  = l->header.seq;

	fromImagetoMat(*l, &this->imgL, l->encoding);
	fromImagetoMat(*r, &this->imgR, r->encoding);
}

void Settings::left_image_callback(const ImageConstPtr& img)
{
	imgSize.height = img->height;
	imgSize.width = img->width;

	int frame  = img->header.seq;

	fromImagetoMat(*img, &this->imgL, img->encoding);
}

void Settings::right_image_callback(const ImageConstPtr& img)
{
	imgSize.height = img->height;
	imgSize.width = img->width;

	int frame  = img->header.seq;

	fromImagetoMat(*img, &this->imgR, img->encoding);
}

void Settings::write(cv::FileStorage& fs) const
//Write serialization for this class
{
	fs << "{"
			<< "BoardSize_Width"  << boardSize.width
			<< "BoardSize_Height" << boardSize.height
			<< "Square_Size"         << squareSize
			<< "Calibrate_Pattern" << patternToUse
			<< "Calibrate_NrOfFrameToUse" << nrFrames
			<< "Write_DetectedFeaturePoints" << writePoints
			<< "Write_extrinsicParameters"   << writeExtrinsics
			<< "Show_UndistortedImage" << showUndistorsed
			<< "Input_FlipAroundHorizontalAxis" << flipVertical
			<< "Input_Delay" << delay
			<< "Input" << input
			<< "}";
}


void Settings::read(const cv::FileNode& node)
//Read serialization for this class
{
	node["BoardSize_Width" ] >> boardSize.width;
	node["BoardSize_Height"] >> boardSize.height;
	node["Calibrate_Pattern"] >> patternToUse;
	node["Square_Size"]  >> squareSize;
	node["Calibrate_NrOfFrameToUse"] >> nrFrames;
	node["Write_DetectedFeaturePoints"] >> writePoints;
	node["Write_extrinsicParameters"] >> writeExtrinsics;
	node["Input_FlipAroundHorizontalAxis"] >> flipVertical;
	node["Show_UndistortedImage"] >> showUndistorsed;
	node["Input"] >> input;
	node["Input_Delay"] >> delay;
	validate();
}


void Settings::validate()
{
	goodInput = true;
	if (boardSize.width <= 0 || boardSize.height <= 0)
	{
		std::cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << std::endl;
		goodInput = false;
	}
	if (squareSize <= 10e-6)
	{
		std::cerr << "Invalid square size " << squareSize << std::endl;
		goodInput = false;
	}
	if (nrFrames <= 0)
	{
		std::cerr << "Invalid number of frames " << nrFrames << std::endl;
		goodInput = false;
	}

	if (input.empty())      // Check for valid input
	{
		inputType = INVALID;
	}
	else
	{
		if (input[0] == 'R')
		{
			inputType = ROS;
		}
		else if (input[0] >= '0' && input[0] <= '9')
		{
			int aux;
			std::stringstream ss(input);
			ss >> aux;

			if (aux < 10)
			{
				cameraID = aux;
			}
			else
			{
				cameraIDL = aux / 10;
				cameraIDR = aux % 10;
			}
			inputType = CAMERA;
		}
		else
		{
			switch (calib_type)
			{
			case Settings::LEFT:
				input = input.substr(0,(input.size()-4)) + "Left.xml";
				std::cout << input << std::endl;
				break;
			case Settings::RIGHT:
				input = input.substr(0,(input.size()-4)) + "Right.xml";
				break;
	}
			if (readStringList(input, imageList))
			{
				inputType = IMAGE_LIST;
				nrFrames = (nrFrames < (int)imageList.size()) ? nrFrames : (int)imageList.size();
			}
			else
			{
				inputType = VIDEO_FILE;
			}
		}

		if (inputType == ROS)
		{
			if(calib_type == STEREO)
		   	{
//				imgL_sub = new message_filters::Subscriber<sensor_msgs::Image>(n, "/image/left/rgb", 1);
//			   	imgR_sub = new message_filters::Subscriber<sensor_msgs::Image>(n, "/image/right/rgb", 1);
				imgL_sub = new message_filters::Subscriber<sensor_msgs::Image>(n, "/uralp/camera_left/image", 1);
				imgR_sub = new message_filters::Subscriber<sensor_msgs::Image>(n, "/uralp/camera_right/image", 1);
			   	sync = new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(10), *imgL_sub, *imgR_sub);
			   	sync->registerCallback(boost::bind(&Settings::stereo_images_callback, this, _1, _2));
           	}
           	else if(calib_type == LEFT)
			{
				image_transport::ImageTransport it(this->n);
                imgL_itsub = it.subscribe("/image/left/rgb_rect", 1, &Settings::left_image_callback, this);
				//imgL_itsub = it.subscribe("/uralp/camera_left/image", 1, &Settings::left_image_callback, this);
			}
           	else if(calib_type == RIGHT)
			{	
				image_transport::ImageTransport it(this->n);
//              	imgR_itsub = it.subscribe("/image/right/rgb", 1, &Settings::right_image_callback, this);
				imgR_itsub = it.subscribe("/uralp/camera_right/image", 1, &Settings::right_image_callback, this);
		    }
		}
		else if (inputType == CAMERA)
		{
			if(calib_type == STEREO)
			{
				inputCaptureL.open(cameraIDL);
				inputCaptureR.open(cameraIDR);
				if (!inputCaptureL.isOpened() || !inputCaptureR.isOpened())
					inputType = INVALID;
			}
			else
			{
				inputCapture.open(cameraID);
				if (!inputCapture.isOpened())
					inputType = INVALID;
			}
		}
		else if (inputType == VIDEO_FILE)
		{
			inputCapture.open(input);
			if (!inputCapture.isOpened())
				inputType = INVALID;
		}
	}
	if (inputType == INVALID)
	{
		std::cerr << " Input does not exist: " << input;
		goodInput = false;
	}

	calibrationPattern = NOT_EXISTING;
	if (!patternToUse.compare("CHESSBOARD")) calibrationPattern = CHESSBOARD;
	if (!patternToUse.compare("CIRCLES_GRID")) calibrationPattern = CIRCLES_GRID;
	if (!patternToUse.compare("ASYMMETRIC_CIRCLES_GRID")) calibrationPattern = ASYMMETRIC_CIRCLES_GRID;
	if (calibrationPattern == NOT_EXISTING)
	{
		std::cerr << " Camera calibration mode does not exist: " << patternToUse << std::endl;
		goodInput = false;
	}
	atImageList = 0;

}
cv::Mat Settings::nextImage()
{
	cv::Mat result;

	if (inputType == CAMERA || inputType == VIDEO_FILE)
	{
		if( inputCapture.isOpened() )
		{
			cv::Mat view0;
			inputCapture >> view0;
			view0.copyTo(result);
		}
	}
	else if (inputType == IMAGE_LIST)
	{
		if( atImageList < imageList.size())
		{
			result = imread(imageList[atImageList++], cv::IMREAD_COLOR);

		}
	}
	else if (inputType == ROS)
	{
		switch (calib_type)
		{
		case LEFT:
			result = imgL;
			break;
		case RIGHT:
			result = imgR;
			break;
		}
	}
	return result;
}

void Settings::nextImageStereo(cv::Mat& imL, cv::Mat& imR)
{
	imL.release();
	imR.release();

	if (inputType == CAMERA || inputType == VIDEO_FILE)
	{
		if( inputCaptureL.isOpened() && inputCaptureR.isOpened())
		{
			cv::Mat view0L;
			cv::Mat view0R;
			inputCaptureL >> view0L;
			inputCaptureR >> view0R;
			view0L.copyTo(imL);
			view0R.copyTo(imR);
		}
	}
	else if (inputType == IMAGE_LIST)
	{
		std::cout << imageList.size() << std::endl;

		if( atImageList < imageList.size()/2 )
		{
			imL = imread(imageList[atImageList], cv::IMREAD_COLOR);
			imR = imread(imageList[imageList.size()/2 + atImageList], cv::IMREAD_COLOR);
			atImageList++;
		}
	}
	else if (inputType == ROS)
	{
		imL = imgL;
		imR = imgR;
	}
}

bool Settings::readStringList( const std::string& filename, std::vector<std::string>& l )
{
	l.clear();
	cv::FileStorage fs(filename, cv::FileStorage::READ);
	if( !fs.isOpened() )
		return false;
	cv::FileNode n = fs.getFirstTopLevelNode();
	if( n.type() != cv::FileNode::SEQ )
		return false;
	cv::FileNodeIterator it = n.begin(), it_end = n.end();
	for( ; it != it_end; ++it )
		l.push_back((std::string)*it);
	return true;
}


