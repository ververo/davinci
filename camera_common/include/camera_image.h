/**
 * @file     camimage.h
 * @author   Jesús Ortiz
 * @version  1.0
 * @date     02-Dec-2013
 * @brief    Image class for use with the cameras
 */

#ifndef CAMIMAGE_H_
#define CAMIMAGE_H_

/*
 * INCLUDE
 */

#include <stdlib.h>
#include <iostream>
#include <string.h>

#include "camaux.h"


/*
 * CLASS: CamImage
 */

/** Image class to use with the cameras */
class CamImage
{
protected:
  int width;
  int height;
  int size;
  int bytesPerPixel;
  unsigned char *buffer;

public:
  /** Constructor */
  CamImage();

  /** Destructor */
  ~CamImage();

  /** Resize the buffer */
  void resize(int width, int height);

  /** Get the width */
  int getWidth();

  /** Get the height */
  int getHeight();

  /** Get the buffer size */
  int getSize();

  /** Get the number of bytes per pixel */
  int getBytesPerPixel();

  /** Get the buffer pointer */
  unsigned char *getBuffer();

  /** Copy data from another image */
  void copy(CamImage &image);
};

#endif  /* CAMIMAGE_H_ */
