/*
 * cam_davinci.h
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica
 */

#ifndef CAM_DAVINCI_SRC_CAM_DAVINCI_H_
#define CAM_DAVINCI_SRC_CAM_DAVINCI_H_



/*
 * INCLUDE
 */
  /* Camera common */
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>

#include "camera_image.h"
//#undef HAVE_OPENCV_CUDAWARPING //this can be used to test without cuda
#ifdef HAVE_OPENCV_CUDAWARPING
// TODO
// this was introduced because those retarded in Willow Garage forgot it
using namespace std;
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/cudawarping.hpp>
#endif




/*
 * CLASS: Cam_interface
 */

/** Interface class for the camera */
class Cam_interface
{

protected:
  /** Last acquired image */
  CamImage image;

  /** Rectified image */
  CamImage imageRect;

  /** Mutex to lock the image */
  pthread_mutex_t mutexImage;

  /** Grabbing thread */
  pthread_t grabThread;

  /** The grab thread is running */
  bool grabRunning;

    /** @name Rectification variables */
  //! {

  bool doRectification;  //!< Flag to activate the rectification
  cv::Mat K;    //!< Camera matrix (3x3)
  cv::Mat D;    //!< Distortion coefficients (1xN)
  cv::Mat R;    //!< Rotation matrix (3x3)
  cv::Mat P;    //!< Projection matrix (3x4)
#ifdef HAVE_OPENCV_CUDAWARPING
  cv::cuda::GpuMat gpu_uMapX;  //!< Undistortion map X on GPU
  cv::cuda::GpuMat gpu_uMapY;  //!< Undistortion map Y on GPU
#else
  cv::Mat uMapX;  //!< Undistortion map X
  cv::Mat uMapY;  //!< Undistortion map Y
#endif
  //! }


public:
  /** Constructor */
  Cam_interface();

  /** Destructor */
    virtual ~Cam_interface();

  /** Init camera */
  virtual bool initCamera(const std::string &config) = 0;

  /** Finish camera */
  virtual bool finishCamera() = 0;

  /** Grabs an image from the camera
   *  This function should pull a new image from the camera driver
   *  and save it in the internal image variable
   * @return        True if the image was acquired,
   *             false otherwise
   */
  virtual bool grabImage() = 0;

  /** Set the rectification parameters and calculate the rectification map
   * @param K    Camera matrix (3x3)
   * @param D    Distortion coefficients (1xN)
   * @param R    Rotation matrix (3x3)
   * @param P    Projection matrix (3x4)
   */
  void setRectifyParams(
    float *K,
    float *D, int Dsize,
    float *R,
    float *P,
    int width, int height);

  /** Load the rectification params from a YAML file
   * @param paramsFile  Path to the parameters YAML file
   * @return        True if the parameters were loaded correctly,
   *             False otherwise
   */
  bool loadRectifyParams(const std::string &paramsFile);

  /** Load the rectification params from the standard XML file
   * @param   isLeft  True if left rectification must be performed, false if right
   * @return True if the parameters were loaded correctly, false otherwise
   */
  bool loadRectifyParams(const bool& isLeft);


  /** Rectify the image from the camera */
  void rectify();

  /** Gets the last image from the camera
   *  This function just copies the last acquired image, but doesn't
   *  get a new image. To get the last image from the driver you have
   *  to call the grabImage function.
   * @param img [out]    Structure where to put the last acquired frame
   * @return        True if the image is ok,
   *             false otherwise
   */
  bool getImage(CamImage &img);

  /** Start grabbing images */
  void grabStart();

  /** Stop grabbing images */
  void grabStop();

private:
  /** Grab thread */
  static void *grabRun(void *data);

public:
  /** Locks the access to the image */
  void lockImage();

  /** Unlocks the access to the image */
  void unlockImage();

};


#endif /* CAM_DAVINCI_SRC_CAM_DAVINCI_H_ */
