#include "../include/camaux.h"

pthread_mutex_t camAux_mutex = PTHREAD_MUTEX_INITIALIZER;

// Gets the delta time from the initial time
struct timespec camAux_getTime()
{
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  return now;
}

// Gets the seconds
double camAux_sec(struct timespec time)
{
  return (double)time.tv_sec + (double)time.tv_nsec * 1E-9;
}

// Gets the miliseconds
unsigned long camAux_msec(struct timespec time)
{
  return time.tv_sec * 1E3 + time.tv_nsec / 1E6;
}

// Gets the microseconds
unsigned long camAux_usec(struct timespec time)
{
  return time.tv_sec * 1E6 + time.tv_nsec / 1E3;
}

// Print message for profiling
void camAux_print(const std::string &msg)
{
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  pthread_mutex_lock(&camAux_mutex);
  std::cout << std::setw(16) << std::setfill(' ') << msg << ": ";
  std::cout << std::setw(9) << std::setfill('0') << now.tv_sec << ".";
  std::cout << std::setw(9) << std::setfill('0') << now.tv_nsec << std::endl;
  pthread_mutex_unlock(&camAux_mutex);
}

// Print message in CSV format for profiling
void camAux_printCSV(unsigned int id)
{
  return;
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  pthread_mutex_lock(&camAux_mutex);
  std::cout << std::setw(4) << std::setfill('0') << id << ",";
  std::cout << std::setw(9) << std::setfill('0') << now.tv_sec << ",";
  std::cout << std::setw(9) << std::setfill('0') << now.tv_nsec << std::endl;
  pthread_mutex_unlock(&camAux_mutex);
}
