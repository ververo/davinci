/**
 * @file     camimage.cpp
 * @author   Jesús Ortiz
 * @version  1.0
 * @date     02-Dec-2013
 * @brief    Image class for use with the cameras
 */

/*
 * INCLUDE
 */

#include "camera_image.h"

/*
 * CLASS: CamImage
 */

// Constructor
CamImage::CamImage()
{
  this->width = 0;
  this->height = 0;
  this->size = 0;
  this->bytesPerPixel = 3;
  this->buffer = NULL;
}

// Destructor
CamImage::~CamImage()
{
  if (this->size != 0)
  {
    delete [] this->buffer;
    
    this->size = 0;
    this->width = 0;
    this->height = 0;
  }
}

// Resize the buffer
void CamImage::resize(int width, int height)
{
  if (width <= 0 || height <= 0)
    return;

  if (this->width == width && this->height == height)
    return;

  if (this->size != 0)
    delete [] this->buffer;

  this->width = width;
  this->height = height;
  this->size = width * height * this->bytesPerPixel;
  this->buffer = new unsigned char[this->size];
}

// Get the width
int CamImage::getWidth()
{
  return this->width;
}

// Get the height
int CamImage::getHeight()
{
  return this->height;
}

// Get the buffer size
int CamImage::getSize()
{
  return this->size;
}

// Get the number of bytes per pixel
int CamImage::getBytesPerPixel()
{
  return this->bytesPerPixel;
}

// Get the buffer pointer
unsigned char *CamImage::getBuffer()
{
  return this->buffer;
}

// Copy data from another image
void CamImage::copy(CamImage &image)
{
  if (image.getSize() == 0)
    return;

  if (image.getBytesPerPixel() != this->getBytesPerPixel())
    return;

  this->resize(image.getWidth(), image.getHeight());
  memcpy(this->getBuffer(), image.getBuffer(), this->getSize());
}
