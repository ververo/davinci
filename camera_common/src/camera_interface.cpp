/*
 * cam_davinci.cpp
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica Penza
 */


/*
 * INCLUDE
 */

#include "../include/camera_interface.h"

#include <camera_msgs/paths.h>

/* YAML */
#include <yaml-cpp/yaml.h>

/*
 * YAML operators
 */

void operator >> (const YAML::Node &node, std::vector<float> &floatArray)
{
  floatArray.clear();

  for (unsigned i = 0; i < node.size(); i++)
    floatArray.push_back(node[i].as<float>());
}


/*
 * CLASS: Cam_interface
 */

// Constructor
Cam_interface::Cam_interface()
{
  pthread_mutex_init(&this->mutexImage, NULL);
  this->grabRunning = false;
  this->doRectification = false;
}

// Destructor
Cam_interface::~Cam_interface()
{
}

// Set the rectification parameters and calculate the rectification map
void Cam_interface::setRectifyParams(
  float *K,
  float *D, int Dsize,
  float *R,
  float *P,
  int width, int height)
{
  // Check rectification params
  if (K == NULL || D == NULL || R == NULL || P == NULL)
  {
    this->doRectification = false;
    return;
  }

    //calibration of camera
  this->K = cv::Mat(3, 3, CV_32F, K);
  this->D = cv::Mat(1, Dsize, CV_32F, D);
  this->R = cv::Mat(3, 3, CV_32F, R);
  this->P = cv::Mat(3, 4, CV_32F, P);

  std::cout << "K: " << this->K << std::endl;
  std::cout << "D: " << this->D << std::endl;
  std::cout << "R: " << this->R << std::endl;
  std::cout << "P: " << this->P << std::endl;

  std::cout << "Computes undistortion ..." << std::endl;

  //compute undistortion map
  cv::Mat undistortionMap[2];
  cv::initUndistortRectifyMap(
  this->K, this->D, this->R, this->P,
  cv::Size(width, height),
  CV_32FC1,
  undistortionMap[0], undistortionMap[1]);

#ifdef HAVE_OPENCV_CUDAWARPING
    this->gpu_uMapX.upload(undistortionMap[0]);
    this->gpu_uMapY.upload(undistortionMap[1]);
#else
  undistortionMap[0].copyTo(this->uMapX);
  undistortionMap[1].copyTo(this->uMapY);
#endif

  std::cout << "...undistortion computed" << std::endl;

  this->doRectification = true;
}

// Load the rectification params from a YAML file
bool Cam_interface::loadRectifyParams(const std::string &paramsFile)
{
    /* Variables */

  std::vector<float> K;
  std::vector<float> D;
  std::vector<float> R;
  std::vector<float> P;
  int width;
  int height;


    /* Parse file */

  YAML::Node doc;
  try {
    doc = YAML::LoadFile(paramsFile);
  } catch(YAML::BadFile) {
    return false;
  }
  if(doc.IsNull())
    return false;

  doc["K"] >> K;
  doc["D"] >> D;
  doc["R"] >> R;
  doc["P"] >> P;
  width = doc["width"].as<int>();
  height = doc["height"].as<int>();

    /* Check vector sizes */

  if (K.size() != 9 ||
      R.size() != 9 ||
      P.size() != 12)
    return false;


    /* Calculate rectification */

  this->setRectifyParams(
    &K.front(),
    &D.front(), D.size(),
    &R.front(),
    &P.front(),
    width, height);


    /* Exit */

  return true;
}

// Load the rectification params from the standard XML file
bool Cam_interface::loadRectifyParams(const bool& isLeft)
{
  cv::FileStorage fileStorage;
  fileStorage.open(RECTIFICATION_FILE, cv::FileStorage::READ);

  if(!fileStorage.isOpened()) {
      std::cerr << "Failed to open " << RECTIFICATION_FILE << std::endl;
      this->doRectification = false;
      return false;
  }

  cv::Mat rmap0, rmap1;

  if(isLeft){
      fileStorage["rmap0_left"] >> rmap0;
      fileStorage["rmap1_left"] >> rmap1;
  } else {
      fileStorage["rmap0_right"] >> rmap0;
      fileStorage["rmap1_right"] >> rmap1;
  }

  fileStorage.release();

#ifdef HAVE_OPENCV_CUDAWARPING
  if(rmap0.type() != CV_32FC1 || rmap1.type() != CV_32FC1) {
      std::cerr << "Wrong format for rmap0 or rmap1, converting them" << std::endl;

      cv::Mat correctedRmap0, correctedRmap1;

      cv::convertMaps(rmap0, rmap1, correctedRmap0, correctedRmap1, CV_32FC1);

      this->gpu_uMapX.upload(correctedRmap0);
      this->gpu_uMapY.upload(correctedRmap1);
  } else {
      this->gpu_uMapX.upload(rmap0);
      this->gpu_uMapY.upload(rmap1);
  }

#else
  rmap0.copyTo(this->uMapX);
  rmap1.copyTo(this->uMapY);
#endif

  std::cout << "Undistortion maps imported correctly" << std::endl;

  this->doRectification = true;

  return true;
}

// Rectify the image from the camera
void Cam_interface::rectify()
{
  if(this->doRectification &&
#ifdef HAVE_OPENCV_CUDAWARPING
          (image.getHeight() != gpu_uMapX.size().height ||
           image.getWidth() != gpu_uMapX.size().width ||
           image.getHeight() != gpu_uMapY.size().height ||
           image.getWidth() != gpu_uMapY.size().width)
#else
          (image.getHeight() != uMapX.size().height ||
           image.getWidth() != uMapX.size().width ||
           image.getHeight() != uMapY.size().height ||
           image.getWidth() != uMapY.size().width)
#endif
          ){
      std::cerr << "Received an image with the size different from the rectification matrix." << std::endl
                << "Rectification is not possible and will be disabled." << std::endl;
      this->doRectification = false;
  }
  if (this->doRectification)
  {
    cv::Mat imageMat = cv::Mat(
      this->image.getHeight(), this->image.getWidth(),
      CV_8UC3, this->image.getBuffer());
    cv::Mat imageRectMat = cv::Mat(
      this->image.getHeight(), this->image.getWidth(), CV_8UC3);

#ifdef HAVE_OPENCV_CUDAWARPING
    cv::cuda::GpuMat image_GPU;
    cv::cuda::GpuMat imageRect_GPU;
    image_GPU.upload(imageMat);
    cv::cuda::remap(image_GPU, imageRect_GPU,
                    this->gpu_uMapX, this->gpu_uMapY, cv::INTER_LINEAR);
    imageRect_GPU.download(imageRectMat);
//    cv::cuda::remap(imageMat, imageRectMat,
//                    this->uMapX, this->uMapY, cv::INTER_LINEAR);
#else
    cv::remap(imageMat, imageRectMat,
              this->uMapX, this->uMapY, cv::INTER_LINEAR);
#endif

    this->lockImage();
    this->imageRect.resize(
      this->image.getWidth(), this->image.getHeight());
    memcpy(
      this->imageRect.getBuffer(),
      imageRectMat.data,
      this->imageRect.getSize());
    this->unlockImage();
  }
  else
  {
    this->lockImage();
    this->imageRect.copy(this->image);
    this->unlockImage();
  }
}

// Gets the last image from the camera
bool Cam_interface::getImage(CamImage &img)
{
  camAux_printCSV(201);
  this->lockImage();
  camAux_printCSV(202);

  if (this->imageRect.getSize() == 0)
  {
    this->unlockImage();
    camAux_printCSV(203);
    return false;
  }

  img.resize(this->imageRect.getWidth(), this->imageRect.getHeight());
  memcpy(
    img.getBuffer(),
    this->imageRect.getBuffer(),
    this->imageRect.getSize());
  this->unlockImage();
  camAux_printCSV(203);
  return true;
}

// Start grabbing images
void Cam_interface::grabStart()
{
  if (this->grabRunning)
    return;

  this->grabRunning = true;
  pthread_create(&this->grabThread, NULL, this->grabRun, this);
}

// Stop grabbing images
void Cam_interface::grabStop()
{
  if (!this->grabRunning)
    return;

  this->grabRunning = false;
  pthread_join(this->grabThread, NULL);
}

// Grab thread
void *Cam_interface::grabRun(void *data)
{
  Cam_interface *cam = (Cam_interface*)data;

  while (cam->grabRunning)
  {
    cam->grabImage();
  }

  return NULL;
}

// Locks the access to the image
void Cam_interface::lockImage()
{
  pthread_mutex_lock(&this->mutexImage);
}

// Unlocks the access to the image
void Cam_interface::unlockImage()
{
  pthread_mutex_unlock(&this->mutexImage);
}

