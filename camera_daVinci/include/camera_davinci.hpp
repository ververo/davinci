/*
 * camera_davinci.cpp
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica
 */

#ifndef CAMERA_DAVINCI_INCLUDE_CAMERA_DAVINCI_CPP_
#define CAMERA_DAVINCI_INCLUDE_CAMERA_DAVINCI_CPP_


/*
 * INCLUDE
 */

//#include <unistd.h>

//#define PVDECL  //!< Fix for the Pv library
//#define _x64    //!< Fix for the Pv library
//#include <PvApi.h>


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <pthread.h>
#include <unistd.h>
#include "Config_camera.h"
#include "DeckLinkAPI.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "camera_image.h"


#include "ros/ros.h"
#include <camera_msgs/paths.h>


#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>
/*
 * DEFINES
 */

//#define CAM_PROSILICA_NFRAMES  2


/*
 * FRIEND CLASSES
 */

//class CamNode_prosilica;
//class CamNodelet_prosilica;


/*
 * CLASS: Cam_prosilica
 */
class DeckLinkCaptureDelegate : public IDeckLinkInputCallback
{
public:

	/** Mutex to lock the image */
	pthread_mutex_t mutexImage;

	BMDConfigCamera		g_config;
	IDeckLinkInput*	g_deckLinkInput;

	int width_;
	int height_;
	int rowBytes_;
	uchar* buff_frame;

	DeckLinkCaptureDelegate();

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, LPVOID *ppv) { return E_NOINTERFACE; }
	virtual ULONG STDMETHODCALLTYPE AddRef(void);
	virtual ULONG STDMETHODCALLTYPE  Release(void);
	virtual HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(BMDVideoInputFormatChangedEvents, IDeckLinkDisplayMode*, BMDDetectedVideoInputFormatFlags);
	virtual HRESULT STDMETHODCALLTYPE VideoInputFrameArrived(IDeckLinkVideoInputFrame*, IDeckLinkAudioInputPacket*);

	void lockImage();
	void unlockImage();

private:
	int32_t				m_refCount;
};

/** Prosilica camera class */
class Camera_davinci
{
public:

	/** Last acquired image */
	CamImage image;

	HRESULT							result;
	int								idx;

	IDeckLinkIterator*				deckLinkIterator;
	IDeckLink*						deckLink;

	IDeckLinkAttributes*			deckLinkAttributes;
	bool							formatDetectionSupported;

	IDeckLinkDisplayModeIterator*	displayModeIterator;
	IDeckLinkDisplayMode*			displayMode;
	char*							displayModeName;
	BMDDisplayModeSupport			displayModeSupported;

	IDeckLinkConfiguration*			deckLinkConfiguration;

	DeckLinkCaptureDelegate		delegate;

public:

	/** Constructor */
	Camera_davinci();

	/** Destructor */
	~Camera_davinci();

	/** Init camera */
	void initParams(int idx);

	/** Init camera */
	bool initCamera(const std::string &config);

	/** Finish camera */
	bool finishCamera();

	/** Grabs an image from the camera
	 *  This function should pull a new image from the camera driver
	 *  and save it in the internal image variable
	 * @return        True if the image was acquired,
	 *             false otherwise
	 */
	bool grabImage();

	/** Gets the last image from the camera
	 *  This function just copies the last acquired image, but doesn't
	 *  get a new image. To get the last image from the driver you have
	 *  to call the grabImage function.
	 * @param img [out]    Structure where to put the last acquired frame
	 * @return        True if the image is ok,
	 *             false otherwise
	 */
	bool getImage(CamImage &img);

	static unsigned char clip(int v);

	static void yuv444torgb888(
			unsigned char y,
			unsigned char u,
			unsigned char v,
			unsigned char *rgb);

	static void yuv422torgb24(
			unsigned char *yuv422,
			unsigned char *rgb);

	static void convertyuv422torgb24(
			unsigned char *bufferYUV,
			int lenBufferYUV,
			unsigned char *bufferRGB,
			int lenBufferRGB);

	static void toROSImage(CamImage &img, sensor_msgs::Image &img_);
};

#endif /* CAMERA_DAVINCI_INCLUDE_CAMERA_DAVINCI_CPP_ */
