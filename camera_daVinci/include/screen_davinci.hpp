/*
 * screen_davinci.cpp
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica
 */

#ifndef SCREEN_DAVINCI_INCLUDE_CPP_
#define SCREEN_DAVINCI_INCLUDE_CPP_


/*
 * INCLUDE
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <pthread.h>
#include <unistd.h>
#include "Config_screen.h"
#include "DeckLinkAPI.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include "camera_image.h"

#include "ros/ros.h"
#include <camera_msgs/paths.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <image_transport/image_transport.h>


/** Screen davinci class */
class Screen_davinci
{
private:

	BMDConfigScreen			m_config;

	IDeckLinkVideoFrame*	m_videoFrame;
	IDeckLinkOutput*		m_deckLinkOutput;

	IDeckLink*				m_deckLink;
	IDeckLinkDisplayMode*	m_displayMode;

	unsigned long			m_frameWidth;
	unsigned long			m_frameHeight;
	BMDTimeValue			m_frameDuration;
	BMDTimeScale			m_frameTimescale;
	unsigned long			m_framesPerSecond;

	IDeckLinkIterator*				deckLinkIterator;
	IDeckLinkDisplayModeIterator*	displayModeIterator;
	char*							displayModeName;

public:

	/** Constructor */
	Screen_davinci();

	/** Destructor */
	~Screen_davinci();

	/** Init parameters */
	void initParams(IDeckLink* deckLink);

	/** Init */
	bool init(const std::string &config);

	/** Display a new image */
	bool display(const sensor_msgs::ImageConstPtr &img_);

	/** Finish */
	bool finish();

private:
	bool fromROS(const sensor_msgs::ImageConstPtr &img_, IDeckLinkVideoFrame** frame);

	int GetBytesPerPixel(BMDPixelFormat pixelFormat);

	static unsigned char clip(int v);

	static void rgb444toyuv444(
			unsigned char *rgb,
			unsigned char y,
			unsigned char u,
			unsigned char v);

	static void rgb24toyuv422(
			unsigned char *rgb,
			unsigned char *yuv422);

	static void convertrgb24toyuv422(
			unsigned char *bufferRGB,
			int lenBufferRGB,
			unsigned char *bufferYUV,
			int lenBufferYUV);
};

#endif /* SCREEN_DAVINCI_INCLUDE_CPP_ */
