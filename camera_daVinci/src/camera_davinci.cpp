/*
 * camera_davinci.cpp
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica
 */

/*
 * INCLUDE
 */

#include "camera_davinci.hpp"


/*
 * CLASS: Cam_prosilica
 */
DeckLinkCaptureDelegate::DeckLinkCaptureDelegate() : m_refCount(1)
{
	pthread_mutex_init(&this->mutexImage, NULL);
}

ULONG DeckLinkCaptureDelegate::AddRef(void)
{
	return __sync_add_and_fetch(&m_refCount, 1);
}

ULONG DeckLinkCaptureDelegate::Release(void)
{
	int32_t newRefValue = __sync_sub_and_fetch(&m_refCount, 1);
	if (newRefValue == 0)
	{
		delete this;
		return 0;
	}
	return newRefValue;
}

HRESULT DeckLinkCaptureDelegate::VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame)
{
	void* frameBytes;
	// Handle Video Frame
	if (videoFrame)
	{
		if (videoFrame->GetFlags() & bmdFrameHasNoInputSource)
		{
			std::cout << "[ERROR] No input source was detected -- frame is invalid" << std::endl;
			return E_UNEXPECTED;
		}
		else
		{
			// copy the frame on the local buffer
			if (videoFrame->GetHeight() <= 0 || videoFrame->GetWidth() <= 0)
				return E_UNEXPECTED;

			int size = videoFrame->GetRowBytes() * videoFrame->GetHeight();

			this->lockImage();

			if (videoFrame->GetWidth() != this->width_ || videoFrame->GetHeight() !=  this->height_)
			{
				if( this->buff_frame != NULL)
					delete [] this->buff_frame;

				this->buff_frame = new unsigned char[size];
			}

			videoFrame->GetBytes(&frameBytes);
			memcpy( this->buff_frame,
					(unsigned char*)frameBytes,
					size);

			this->width_ = videoFrame->GetWidth();
			this->height_ = videoFrame->GetHeight();
			this->rowBytes_ = videoFrame->GetRowBytes();

			this->unlockImage();
		}
	}

	return S_OK;
}

HRESULT DeckLinkCaptureDelegate::VideoInputFormatChanged(BMDVideoInputFormatChangedEvents events, IDeckLinkDisplayMode *mode, BMDDetectedVideoInputFormatFlags formatFlags)
{
	// This only gets called if bmdVideoInputEnableFormatDetection was set
	// when enabling video input
	HRESULT	result;
	char*	displayModeName = NULL;
	BMDPixelFormat	pixelFormat = bmdFormat10BitYUV;

	if (formatFlags & bmdDetectedVideoInputRGB444)
		pixelFormat = bmdFormat10BitRGB;

	mode->GetName((const char**)&displayModeName);
	printf("Video format changed to %s %s\n", displayModeName, formatFlags & bmdDetectedVideoInputRGB444 ? "RGB" : "YUV");

	if (displayModeName)
		free(displayModeName);

	if (this->g_deckLinkInput)
	{
		this->g_deckLinkInput->StopStreams();

		result = this->g_deckLinkInput->EnableVideoInput(mode->GetDisplayMode(), pixelFormat, this->g_config.m_inputFlags);
		if (result != S_OK)
		{
			fprintf(stderr, "Failed to switch video mode\n");
			goto bail;
		}

		this->g_deckLinkInput->StartStreams();
	}

	bail:
	return S_OK;
}

void DeckLinkCaptureDelegate::lockImage()
{
	pthread_mutex_lock(&this->mutexImage);
}

void DeckLinkCaptureDelegate::unlockImage()
{
	pthread_mutex_unlock(&this->mutexImage);
}


// Constructor
Camera_davinci::Camera_davinci()
{

	this->deckLinkIterator = NULL;
	this->deckLink = NULL;
	this->deckLinkAttributes = NULL;
	this->displayModeIterator = NULL;
	this->displayMode = NULL;
	this->displayModeName = NULL;
	//	delegate = NULL;

}

// Destructor
Camera_davinci::~Camera_davinci()
{
	//  for (int i = 0; i < CAM_PROSILICA_NFRAMES; i++)
	//  {
	//    if (this->frame[i].ImageBufferSize != 0)
	//      delete [] (char*)this->frame[i].ImageBuffer;
	//  }
}

/** Init camera */
void Camera_davinci::initParams(int idx)
{
	// init parameters
	// parameters
	delegate.g_config.m_deckLinkIndex = idx;
	delegate.g_config.m_displayModeIndex = 2;
	//	g_config.m_inputFlags = bmdVideoInputFlagDefault | bmdVideoInputDualStream3D;
	delegate.g_config.m_inputFlags = bmdVideoInputFlagDefault;
	delegate.g_config.m_pixelFormat = bmdFormat8BitYUV;
	//	g_config.m_pixelFormat = bmdFormat10BitYUV;
	//	g_config.m_pixelFormat = bmdFormat10BitRGB;
	delegate.g_config.m_timecodeFormat = bmdTimecodeRP188Any;
	//	g_config.m_timecodeFormat = bmdTimecodeVITC;
	//	g_config.m_timecodeFormat = bmdTimecodeSerial;
	// Get device and display mode names

	IDeckLink* deckLink = delegate.g_config.GetDeckLink(delegate.g_config.m_deckLinkIndex);
	if (deckLink != NULL)
	{
		if (delegate.g_config.m_displayModeIndex != -1)
		{
			IDeckLinkDisplayMode* displayMode = delegate.g_config.GetDeckLinkDisplayMode(deckLink, delegate.g_config.m_displayModeIndex);
			if (displayMode != NULL)
			{
				displayMode->GetName((const char**)&delegate.g_config.m_displayModeName);
				displayMode->Release();
			}
			else
			{
				delegate.g_config.m_displayModeName = strdup("Invalid");
			}
		}
		else
		{
			delegate.g_config.m_displayModeName = strdup("Format Detection");
		}

		deckLink->GetModelName((const char**)&delegate.g_config.m_deckLinkName);
		deckLink->Release();
	}
	else
	{
		delegate.g_config.m_deckLinkName = strdup("Invalid");
	}
}

// Init camera
bool Camera_davinci::initCamera(const std::string &config)
{

	// Get the DeckLink device
	deckLinkIterator = CreateDeckLinkIteratorInstance();
	if (!deckLinkIterator)
	{
		fprintf(stderr, "This application requires the DeckLink drivers installed.\n");
		return false;
	}


	this->idx = delegate.g_config.m_deckLinkIndex;

	while ((result = deckLinkIterator->Next(&deckLink)) == S_OK)
	{

		std::cout << idx << std::endl;
		if (idx == 0)
			break;
		--idx;

		deckLink->Release();
	}

	if (result != S_OK || deckLink == NULL)
	{
		fprintf(stderr, "Unable to get DeckLink device %u\n", delegate.g_config.m_deckLinkIndex);
		return false;
	}


	// Get the input (capture) interface of the DeckLink device
	result = deckLink->QueryInterface(IID_IDeckLinkInput, (void**)&delegate.g_deckLinkInput);
	if (result != S_OK)
		return false;


	result = deckLink->QueryInterface(IID_IDeckLinkAttributes, (void**)&deckLinkAttributes);
	if (result != S_OK)
	{
		deckLinkAttributes = NULL;

	}

	// Get the display mode
	if (delegate.g_config.m_displayModeIndex == -1)
	{
		if (deckLinkAttributes != NULL)
		{
			result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsInputFormatDetection, &formatDetectionSupported);
			if (result != S_OK || !formatDetectionSupported)
			{
				fprintf(stderr, "Format detection is not supported on this device\n");
				return false;
			}
		}
		// Check the card supports format detection
		delegate.g_config.m_inputFlags |= bmdVideoInputEnableFormatDetection;

		// Format detection still needs a valid mode to start with
		idx = 0;
	}
	else
	{
		idx = delegate.g_config.m_displayModeIndex;
	}

	result = delegate.g_deckLinkInput->GetDisplayModeIterator(&displayModeIterator);
	if (result != S_OK)
		return false;

	while ((result = displayModeIterator->Next(&displayMode)) == S_OK)
	{
		if (idx == 0)
			break;
		--idx;

		displayMode->Release();
	}

	if (result != S_OK || displayMode == NULL)
	{
		fprintf(stderr, "Unable to get display mode %d\n", delegate.g_config.m_displayModeIndex);
		return false;
	}

	// Get display mode name
	result = displayMode->GetName((const char**)&displayModeName);
	if (result != S_OK)
	{
		displayModeName = (char *)malloc(32);
		snprintf(displayModeName, 32, "[index %d]", delegate.g_config.m_displayModeIndex);
	}

	// Check display mode is supported with given options
	result = delegate.g_deckLinkInput->DoesSupportVideoMode(displayMode->GetDisplayMode(),
			delegate.g_config.m_pixelFormat, bmdVideoInputFlagDefault, &displayModeSupported, NULL);
	if (result != S_OK)
		return false;

	if (displayModeSupported == bmdDisplayModeNotSupported)
	{
		fprintf(stderr, "The display mode %s is not supported with the selected pixel format\n", displayModeName);
		return false;
	}

	if (delegate.g_config.m_inputFlags & bmdVideoInputDualStream3D)
	{
		if (!(displayMode->GetFlags() & bmdDisplayModeSupports3D))
		{
			fprintf(stderr, "The display mode %s is not supported with 3D\n", displayModeName);
			return false;
		}
	}

	// Print the selected configuration
	delegate.g_config.DisplayConfiguration();

	//Set Half Duplex Mode
	bool supportDuplex;

	deckLinkAttributes->GetFlag(BMDDeckLinkSupportsDuplexModeConfiguration, &supportDuplex);
	if(supportDuplex) {

		deckLink->QueryInterface(IID_IDeckLinkConfiguration, (void**)&deckLinkConfiguration);
		deckLinkConfiguration->SetInt(bmdDeckLinkConfigDuplexMode, bmdDuplexModeHalf);

	}
	else{
		std::cout << "No Duplex Configration Supported" << std::endl;
		deckLink->QueryInterface(IID_IDeckLinkConfiguration, (void**)&deckLinkConfiguration);
		long int duplexMode;
		if (deckLinkConfiguration->GetInt(bmdDeckLinkConfigDuplexMode, &duplexMode) == S_OK)
		{
			if (duplexMode == bmdDuplexModeHalf)
				std::cout << "Duplex mode: half" << std::endl;
			else if (duplexMode == bmdDuplexModeFull)
				std::cout << "Duplex mode: full" << std::endl;
			else
				std::cout << "Duplex mode: unknown" << std::endl;
			std::cout << "Duplex mode: " << duplexMode << std::endl;
		}
		else
		{
			std::cout << "Error getting Duplex mode" << std::endl;
		}
	}

	std::cout << __LINE__<< std::endl;


	// Configure the capture callback
	delegate.g_deckLinkInput->SetCallback(&delegate);
	std::cout << __LINE__<< std::endl;

	// Start capturing
	result = delegate.g_deckLinkInput->EnableVideoInput(displayMode->GetDisplayMode(),
			delegate.g_config.m_pixelFormat, delegate.g_config.m_inputFlags);
	if (result != S_OK)
	{
		fprintf(stderr, "Failed to enable video input. Is another application using the card?\n");
		return false;
	}
	std::cout << __LINE__<< std::endl;

	result = delegate.g_deckLinkInput->StartStreams();
	if (result != S_OK)
		return false;

	std::cout << __LINE__<< std::endl;

	return true;
}

// Finish camera
bool Camera_davinci::finishCamera()
{

	fprintf(stderr, "Stopping Capture\n");
	delegate.g_deckLinkInput->StopStreams();


	if (displayModeName != NULL)
		free(displayModeName);

	if (displayMode != NULL)
		displayMode->Release();

	if (displayModeIterator != NULL)
		displayModeIterator->Release();

	if (delegate.g_deckLinkInput != NULL)
	{
		delegate.g_deckLinkInput->Release();
		delegate.g_deckLinkInput = NULL;
	}

	if (deckLinkAttributes != NULL)
		deckLinkAttributes->Release();

	if (deckLink != NULL)
		deckLink->Release();

	if (deckLinkIterator != NULL)
		deckLinkIterator->Release();
	return true;
}

// Grabs an image from the camera
bool Camera_davinci::grabImage()
{
	delegate.lockImage();

	int width = delegate.width_;
	int height =  delegate.height_;

	this->image.resize(width, height);
	unsigned char *imgData = this->image.getBuffer();

	int frameSize = width * height * 2;

	if( delegate.buff_frame == NULL || imgData == NULL )
	{
		delegate.unlockImage();
		std::cout << "No image available" << std::endl;
		return false;
	}

	Camera_davinci::convertyuv422torgb24(
			(unsigned char*) delegate.buff_frame, frameSize,
			imgData, width * height * 3);

	delegate.unlockImage();

	return true;
}

// Gets the last image from the camera
bool Camera_davinci::getImage(CamImage &img)
{
	if (this->image.getSize() == 0)
		return false;

	img.resize(this->image.getWidth(), this->image.getHeight());
	memcpy(
			img.getBuffer(),
			this->image.getBuffer(),
			this->image.getSize());
	return true;
}

unsigned char Camera_davinci::clip(int v)
{
	if (v <= 0)
		return 0;
	else if (v >= 255)
		return 255;
	else
		return v;
}

void Camera_davinci::yuv444torgb888(
		unsigned char y,
		unsigned char u,
		unsigned char v,
		unsigned char *rgb)
{
	int c = y - 16;
	int d = u - 128;
	int e = v - 128;

	rgb[2] = Camera_davinci::clip((298 * c + 409 * e + 128) >> 8);
	rgb[1] = Camera_davinci::clip((298 * c - 100 * d - 208 * e + 128) >> 8);
	rgb[0] = Camera_davinci::clip((298 * c + 516 * d + 128) >> 8);
}

void Camera_davinci::yuv422torgb24(
		unsigned char *yuv422,
		unsigned char *rgb)
{
	unsigned char u  = yuv422[0];
	unsigned char y1 = yuv422[1];
	unsigned char v  = yuv422[2];
	unsigned char y2 = yuv422[3];

	Camera_davinci::yuv444torgb888(y1, u, v, &rgb[0]);
	Camera_davinci::yuv444torgb888(y2, u, v, &rgb[3]);
}

void Camera_davinci::convertyuv422torgb24(
		unsigned char *bufferYUV,
		int lenBufferYUV,
		unsigned char *bufferRGB,
		int lenBufferRGB)
{
	int posRGB = 0;

	for (int i = 0; i < lenBufferYUV; i += 4)
	{
		if (posRGB > (lenBufferRGB - 6))
			return;

		Camera_davinci::yuv422torgb24(&bufferYUV[i], &bufferRGB[posRGB]);
		posRGB += 6;
	}
}

void Camera_davinci::toROSImage(CamImage &img, sensor_msgs::Image &img_)
{
	img_.height = img.getHeight();
	img_.width = img.getWidth();
	img_.is_bigendian = true;
	img_.step = img.getWidth() * 3;
	img_.data.resize(img.getSize());
	img_.encoding = sensor_msgs::image_encodings::RGB8;
	memcpy(
			(char*)&img_.data[0],
			img.getBuffer(),
			img.getSize());
}
