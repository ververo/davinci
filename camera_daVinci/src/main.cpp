/*
 * main.cpp
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica
 */


/*
 * HEADERS
 */

#include "camera_davinci.hpp"
#include "screen_davinci.hpp"


#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> MySyncPolicy;


Screen_davinci outL;
Screen_davinci outR;


void images_callback(
		const sensor_msgs::ImageConstPtr& l,
		const sensor_msgs::ImageConstPtr& r)
{
	// display images
	outL.display(l);
	outR.display(r);
}


/*
 * MAIN
 */

/** Main function
 */
int main(int argc, char *argv[])
{

	/* Init ROS */

	ros::init(argc, argv, "camera_davinci");

	ros::NodeHandle n;
	image_transport::ImageTransport it(n);
	image_transport::Publisher imgL_pub = it.advertise("/image/left/rgb", 1);
	image_transport::Publisher imgR_pub = it.advertise("/image/rigth/rgb", 1);


	//define subscriber
	message_filters::Subscriber<sensor_msgs::Image> imgL_sub(n, "/image/left/rgb", 1);
	message_filters::Subscriber<sensor_msgs::Image> imgR_sub(n, "/image/right/rgb", 1);


	ros::Rate loop_rate(10);
	Camera_davinci camL;
	Camera_davinci camR;

	CamImage imgL;
	CamImage imgR;
	sensor_msgs::Image imgL_;
	sensor_msgs::Image imgR_;
	sensor_msgs::Image imgL_output_;
	sensor_msgs::Image imgR_output_;

	// ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
	message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(10), imgL_sub, imgR_sub);
	sync.registerCallback(boost::bind(&images_callback, _1, _2));

	std::cout << __LINE__<< std::endl;
	// init card parameters
	camL.initParams(0);
	camR.initParams(1);

	//Init camera
	if (!camL.initCamera(""))
	{
		ROS_ERROR("Error initing Left camera");
		return 0;
	}
	else
	{
		std::cout << "Left Camera Initialized" << std::endl;
	}

	if (!camR.initCamera(""))
	{
		ROS_ERROR("Error initing Right camera");
		return 0;
	}
	else
	{
		std::cout << "Right Camera Initialized" << std::endl;
	}

//	outL.initParams(camL.deckLink);
	//	outR.initParams(3);

	//Init output
//	if (!outL.init(""))
//	{
//		ROS_ERROR("Error initing Left output");
//		return 0;
//	}
//	else
//	{
//		std::cout << "Left Output Initialized" << std::endl;
//	}
//	std::cout << __LINE__<< std::endl;
	//
	//	if (!outR.init(""))
	//	{
	//		ROS_ERROR("Error initing Right output");
	//		return 0;
	//	}

	std::cout << __LINE__<< std::endl;

	while (ros::ok())
	{

		// grab images
		camL.grabImage();
		camR.grabImage();
		//
		//		// save images
		camL.getImage(imgL);
		camR.getImage(imgR);

		//convert read images to ROS
		camL.toROSImage(imgL,imgL_);
		camR.toROSImage(imgR,imgR_);

		//publish the image
		imgL_pub.publish(imgL_);
		imgR_pub.publish(imgR_);

		// Spin
		ros::spinOnce();

		// Sleep a bit
		loop_rate.sleep();
	}

	camL.finishCamera();
	camR.finishCamera();
	//
	//	outL.finish();
	//	outR.finish();

	return 0;
}

