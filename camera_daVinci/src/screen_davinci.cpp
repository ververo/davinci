/*
 * camera_davinci.cpp
 *
 *  Created on: Nov 9, 2016
 *      Author: veronica
 */

/*
 * INCLUDE
 */

#include "screen_davinci.hpp"


// Constructor
Screen_davinci::Screen_davinci()
{
	this->m_deckLink = NULL;
	this->m_displayMode = NULL;
	this->m_frameWidth = 0;
	this->m_frameHeight = 0;
	this->m_frameDuration = 0;
	this->m_frameTimescale = 0;
	this->m_framesPerSecond = 0;
	this->deckLinkIterator = NULL;
	this->displayModeIterator = NULL;
	this->displayModeName = NULL;
	this->m_videoFrame = NULL;
	this->m_deckLinkOutput = NULL;
}

// Destructor
Screen_davinci::~Screen_davinci()
{

}

/** Init camera */
void Screen_davinci::initParams(IDeckLink* deckLink)
{
	std::cout << __LINE__<< std::endl;
	this->m_config.m_deckLinkIndex = 0;
	this->m_config.m_displayModeIndex = 2;
	this->m_config.m_outputFlags = bmdVideoOutputFlagDefault;
	this->m_config.m_pixelFormat = bmdFormat8BitYUV;
	std::cout << __LINE__<< std::endl;
	this->m_deckLink = this->m_config.GetDeckLink(this->m_config.m_deckLinkIndex);
//	this->m_deckLink = deckLink;
	if (this->m_deckLink != NULL)
	{
		std::cout << __LINE__<< std::endl;
		if (this->m_config.m_displayModeIndex != -1)
		{
			std::cout << __LINE__<< std::endl;
			IDeckLinkDisplayMode* displayMode = this->m_config.GetDeckLinkDisplayMode(this->m_deckLink, this->m_config.m_displayModeIndex);
			if (displayMode != NULL)
			{
				displayMode->GetName((const char**)&this->m_config.m_displayModeName);
				displayMode->Release();
			}
			else
			{
				this->m_config.m_displayModeName = strdup("Invalid");
			}
		}
		else
		{
			this->m_config.m_displayModeName = strdup("Format Detection");
		}

		this->m_deckLink->GetModelName((const char**)&this->m_config.m_deckLinkName);
		//this->m_deckLink->Release();
	}
	else
	{
		std::cout << __LINE__<< std::endl;
		this->m_config.m_deckLinkName = strdup("Invalid");
	}
	std::cout << __LINE__<< std::endl;
}

// Init camera
bool Screen_davinci::init(const std::string &config)
{
	std::cout << __LINE__<< std::endl;
	HRESULT							result;
	int								idx;
	bool							success = false;
//	std::cout << __LINE__<< std::endl;
//	// Get the DeckLink device
//	deckLinkIterator = CreateDeckLinkIteratorInstance();
//	if (!deckLinkIterator)
//	{
//		fprintf(stderr, "This application requires the DeckLink drivers installed.\n");
//		return false;
//	}
//	std::cout << __LINE__<< std::endl;
//	idx = m_config.m_deckLinkIndex;
//
//	while ((result = deckLinkIterator->Next(&m_deckLink)) == S_OK)
//	{
//		if (idx == 0)
//			break;
//		--idx;
//
//		m_deckLink->Release();
//	}
//
//	std::cout << "result: " << result << std::endl;
//
	if (m_deckLink == NULL)
	{
		fprintf(stderr, "Unable to get DeckLink device %u\n", m_config.m_deckLinkIndex);
		return false;
	}

	// Get the output (display) interface of the DeckLink device
	if (m_deckLink->QueryInterface(IID_IDeckLinkOutput, (void**)&m_deckLinkOutput) != S_OK)
		return false;
	std::cout << __LINE__<< std::endl;
	// Get the display mode
	idx = m_config.m_displayModeIndex;
	std::cout << __LINE__<< std::endl;
	result = m_deckLinkOutput->GetDisplayModeIterator(&displayModeIterator);
	if (result != S_OK)
		return false;
	std::cout << __LINE__<< std::endl;
	while ((result = displayModeIterator->Next(&m_displayMode)) == S_OK)
	{
		if (idx == 0)
			break;
		--idx;

		m_displayMode->Release();
	}
	std::cout << __LINE__<< std::endl;
	if (result != S_OK || m_displayMode == NULL)
	{
		fprintf(stderr, "Unable to get display mode %d\n", m_config.m_displayModeIndex);
		return false;
	}
	std::cout << __LINE__<< std::endl;
	// Get display mode name
	result = m_displayMode->GetName((const char**)&displayModeName);
	if (result != S_OK)
	{
		displayModeName = (char *)malloc(32);
		snprintf(displayModeName, 32, "[index %d]", m_config.m_displayModeIndex);
	}
	std::cout << __LINE__<< std::endl;
	m_config.DisplayConfiguration();
	std::cout << __LINE__<< std::endl;
	// Provide this class as a delegate to the audio and video output interfaces
	//m_deckLinkOutput->SetScheduledFrameCompletionCallback(&this->delegate);

	m_frameWidth = m_displayMode->GetWidth();
	m_frameHeight = m_displayMode->GetHeight();
	m_displayMode->GetFrameRate(&m_frameDuration, &m_frameTimescale);

	// Calculate the number of frames per second, rounded up to the nearest integer.  For example, for NTSC (29.97 FPS), framesPerSecond == 30.
	m_framesPerSecond = (unsigned long)((m_frameTimescale + (m_frameDuration-1))  /  m_frameDuration);

	// Set the video output mode
	result = m_deckLinkOutput->EnableVideoOutput(m_displayMode->GetDisplayMode(), m_config.m_outputFlags);
	if (result != S_OK)
	{
		fprintf(stderr, "Failed to enable video output. Is another application using the card?\n");
		return false;
	}

	// Schedule first frame
	//delegate.ScheduleNextFrame();

	return true;
}

bool Screen_davinci::display(const sensor_msgs::ImageConstPtr &img_)
{
	// Convert from ROS msg to video frame
	if (!this->fromROS(img_, &this->m_videoFrame))
		return false;

	if (this->m_deckLinkOutput->DisplayVideoFrameSync(this->m_videoFrame) != S_OK)
		return false;

	return true;
}

// Finish camera
bool Screen_davinci::finish()
{
	m_deckLinkOutput->StopScheduledPlayback(0, NULL, 0);
	m_deckLinkOutput->DisableVideoOutput();

	if (m_videoFrame != NULL)
		m_videoFrame->Release();
	m_videoFrame = NULL;

	if (displayModeName != NULL)
		free(displayModeName);

	if (m_displayMode != NULL)
		m_displayMode->Release();

	if (displayModeIterator != NULL)
		displayModeIterator->Release();

	if (m_deckLinkOutput != NULL)
		m_deckLinkOutput->Release();

	if (m_deckLink != NULL)
		m_deckLink->Release();

	if (deckLinkIterator != NULL)
		deckLinkIterator->Release();

	return true;
}

bool Screen_davinci::fromROS(const sensor_msgs::ImageConstPtr &img_, IDeckLinkVideoFrame** frame)
{
	HRESULT	result;
	int bytesPerPixel = GetBytesPerPixel(m_config.m_pixelFormat);
	IDeckLinkMutableVideoFrame*	newFrame = NULL;

	if (img_->width != m_frameWidth || img_->height != m_frameHeight)
		return false;

	result = m_deckLinkOutput->CreateVideoFrame(
			m_frameWidth, m_frameHeight, m_frameWidth * bytesPerPixel,
			m_config.m_pixelFormat, bmdFrameFlagDefault, &newFrame);
	if (result != S_OK)
	{
		fprintf(stderr, "Failed to create video frame\n");
		return false;
	}

	if (m_config.m_pixelFormat != bmdFormat8BitYUV)
		return false;

	unsigned char *bufferNewFrame;
	int sizeNewFrame = newFrame->GetHeight() * newFrame->GetRowBytes();
	newFrame->GetBytes((void**)&bufferNewFrame);

	unsigned char *bufferImg = (unsigned char *)img_->data.data();
	int sizeImg = img_->height * img_->step;

	Screen_davinci::convertrgb24toyuv422(bufferImg, sizeImg, bufferNewFrame, sizeNewFrame);

	*frame = newFrame;
	return true;
}

int Screen_davinci::GetBytesPerPixel(BMDPixelFormat pixelFormat)
{
	int bytesPerPixel = 2;

	switch(pixelFormat)
	{
	case bmdFormat8BitYUV:
		bytesPerPixel = 2;
		break;
	case bmdFormat8BitARGB:
	case bmdFormat10BitYUV:
	case bmdFormat10BitRGB:
		bytesPerPixel = 4;
		break;
	}

	return bytesPerPixel;
}

unsigned char Screen_davinci::clip(int v)
{
	if (v <= 0)
		return 0;
	else if (v >= 255)
		return 255;
	else
		return v;
}

void Screen_davinci::rgb444toyuv444(
		unsigned char *rgb,
		unsigned char y,
		unsigned char u,
		unsigned char v)
{
	int a = (66 * rgb[0] + 129 * rgb[1] + 25 * rgb[2] + 128) >> 8;
	int b = (-38 * rgb[0] - 74 * rgb[1] + 112 * rgb[2] + 128) >> 8;
	int c = (112 * rgb[0] - 94 * rgb[1] - 18 * rgb[2] + 128) >> 8;

	y = Screen_davinci::clip(a + 16);
	u = Screen_davinci::clip(b + 128);
	v = Screen_davinci::clip(c + 128);
}

void Screen_davinci::rgb24toyuv422(
		unsigned char *rgb,
		unsigned char *yuv422)
{
	unsigned char y1;
	unsigned char u1;
	unsigned char v1;
	unsigned char y2;
	unsigned char u2;
	unsigned char v2;

	Screen_davinci::rgb444toyuv444(&rgb[0], y1, u1, v1);
	Screen_davinci::rgb444toyuv444(&rgb[3], y2, u2, v2);

	yuv422[0] = y2;
	yuv422[1] = Screen_davinci::clip((v1 + v2) / 2);
	yuv422[2] = y1;
	yuv422[3] = Screen_davinci::clip((u1 + u2) / 2);
}

void Screen_davinci::convertrgb24toyuv422(
		unsigned char *bufferRGB,
		int lenBufferRGB,
		unsigned char *bufferYUV,
		int lenBufferYUV)
{
	int posYUV = 0;

	for (int i = 0; i < lenBufferRGB; i += 6)
	{
		if (posYUV > (lenBufferYUV - 4))
			return;

		Screen_davinci::rgb24toyuv422(&bufferRGB[i], &bufferYUV[posYUV]);
		posYUV += 4;
	}
}
